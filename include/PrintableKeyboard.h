#ifndef PRINTABLE_KEYBOARD_H
#define PRINTABLE_KEYBOARD_H

#include <limits>
#include <vector>
#include <BasicTypes.h>
#include <OBBTree.h>
#include <boost/filesystem.hpp>

namespace fs=boost::filesystem;

namespace easymotion{
//Given the markers position the printable keyboard defines the set of offsets for all the keys as well as their 
//symbolic meaning. In this class we are going to use the CV classes to define 2D and 3D data types etc. The 
//keyboard symbols will be read in from file where the keyboard's layout and info will be stored. 

//The basic assumption behind the keyboard is that a user's finger can only activate ONE key at a time.

//Here we will follow ALVAR and assume that x is right and y is down.

class PrintableKeyboard{
public:
	PrintableKeyboard();
	PrintableKeyboard(const PrintableKeyboard& iOther);
	PrintableKeyboard& operator=(const PrintableKeyboard& iOther); 

	/// return an invalidKey if no key was pressed or if the key press somehow went wrong. Call the following function
	/// for each finger.
	KeyValue getKeyPressed( const FingerObject& iFinger, const Transformation& iKeyBoardTransf); 

	/// Do the following initializeKeyboard() in a different file, some type of initialization file that starts when the program starts and
	/// loads all keyboards into main memory.

	/// Load the keyboard from some XML file? Potentially when the program starts all saved keyboard layouts can 
	/// be loaded and saved somewhere so that if a marker is detected that corresponds to a given keyboard then
	/// we can use it straight away.
	bool initializeKeyboard(const fs::path& iFileName, const int iKeyboardId);

	/// To check if the keyboard has been initialized.
	bool isValidKeyboard() const;
	/// The following are all self explanatory.
	void setKeyboardId(const int iId);
	int getKeyboardId() const;
	Scalar getKeyHeight() const;
	void setKeyHeight(Scalar iHeight);
	size_t getNKeys() const;
	const std::vector<Key>& getKeys() const;
	const Key& getKey(const int iIndex) const;
	KeyValue getKeyValue(const int iKeyIndex) const;
	KeyValue getInvalidKey() const;
	const std::array<Vec2_type, 4>& getKeyboardCorners() const;
	const OBBTree& getOBBTree() const;
	Scalar getMarkerSize() const;
	Idx_type getKeyValueIdx(const KeyValue iKeyValue) const;

protected:
	/// return whether the given object is colliding with the keyboard and if so the key it is colliding with.
	bool keyboardCollide(KeyValue& oKeyValue, const FingerObject& iFinger, const Transformation& iKeyBoardTransf);
	/// To initialize a keyboard with another keyboard.
	void init(const PrintableKeyboard& iOther);
private:
	/// The size of the marker in centimeters.
	Scalar mMarkerSize;
	/// The vector of keys are the position, orientation, side lengths of each of the keys as well as the symbols stored in each of the keys. Could be an ASCII code or something but it'd be nice to have more flexibility than that
	/// so that we can do different languages.
	std::vector<Key> mKeys;
	/// The mInvalidKey is returned when no key has been pressed
	KeyValue mInvalidKey;
	/// The id of the current keyboard out of all possible keyboards
	int mKeyBoardId;
	/// Has a key been pressed?
	bool mKeyPressed;
	/// The data structure to detect key presses.
	OBBTree mOBBTree;
	/// The four marks so we can transform between devices.
	std::array<Vec2_type, 4> mKeyboardCorners;
	/// Define the height perpendicular to the plane of the keyboard in each direction of our virtual keys.
	Scalar mKeyHeight;
};

} //namespace easymotion

#endif