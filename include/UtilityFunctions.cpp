#include "UtilityFunctions.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

#include "WINStuff.h"

#include <pxcgesture.h>

#define BOOST_FILESYSTEM_NO_DEPRECATED

using namespace std;
using namespace boost::filesystem;

namespace easymotion{

bool isValidKeyboard(const path& iDirAndFileName){
	//Check that the file is a keyboard file and has the right layout etc.
	//Open the file with a iostream and read its contents.
	ifstream file;
	file.exceptions(ios::failbit | ios::badbit);
	file.open(iDirAndFileName.c_str(), ios::in);
	if(!file)
		return false;
	if(file.peek() == ifstream::traits_type::eof())
		return false;
	string line, token;
	getline(file, line);
	file.close();
	{
		istringstream input(line);
		//Get the first item in the string line.
		input >> token;
		if(token.compare("keyboard") == 0) { 
			return true;
		}
		return false;
	}
}

//Use BOOST.FileSystem to enumerate through the directory. Do not do this myself!!
size_t enumerateKeyboards(std::vector< path >& oFilePaths, const path& iDirectoryName){
	try
	{
		if (exists(iDirectoryName))    // does p actually exist?
		{
			if (is_directory(iDirectoryName))      // is p a directory?
			{
				for(auto it = directory_iterator(iDirectoryName); it != directory_iterator(); ++it)
				{
					path fn = it->path().filename();   // extract the filename from the path
					auto DirAndFile = iDirectoryName /  fn;
					if(isValidKeyboard(DirAndFile))
						oFilePaths.push_back(DirAndFile);                 
				}
			}
			else
				cout << iDirectoryName << " exists, but is not a directory\n";
		}
		else
			cout << iDirectoryName << " does not exist\n";
	}
	catch (const filesystem_error& ex)
	{
		cout << ex.what() << '\n';
	}
	return oFilePaths.size();
}

bool getNewDepthValue(int& oCornerDepth, 
					  const unsigned char* iDepth_data_plane, 
					  const unsigned int iDepthWidth, 
					  const unsigned int iDepthHeight, 
					  const Vec2i_type& iDepthCornerPoint,
					  const std::array<float, 2>& iDepthBadValues ){
	
	const int maxDepthIndex = iDepthWidth*iDepthHeight-1;
	int drx = -2;
	for(; drx<=2; drx++ ){
		int dry = -2;
		int maxDry = 2;
		if(iDepthCornerPoint[1] <= 1){
			dry=0;
			maxDry += (2-iDepthCornerPoint[1]);
		}
		for(; dry<=maxDry; dry++ ){
			int index = (iDepthCornerPoint[1]+dry)*iDepthWidth+(iDepthCornerPoint[0]+drx);
			//Make sure we don't go off the edge of the depth picture.
			index = std::max(0, index);
			index = std::min(maxDepthIndex, index);
			const auto depth = ((short*)iDepth_data_plane)[index];
			if(depth != (short)iDepthBadValues[0] && depth != (short)iDepthBadValues[1]){
				oCornerDepth = (int)depth;
				return true;
			}
		}
	}
	return false;
}

void WINprocessResult(const HandMouseResult& iResult){
	
	switch(iResult.mClickAction){
		case HandMouseResult::SINGLE_CLICK:
			//Do a click
			LeftClick();
		break;
		case HandMouseResult::START_CLICK: 
			LeftHoldClick();
		break;
		case HandMouseResult::HELD_CLICK:
			//Whatever is clicked on move it iResult.mMouseMovement
			MoveMouse(iResult.mMouseMovement);
		break;
		case HandMouseResult::STOP_CLICK:
			LeftUnClick();
		break;
		case HandMouseResult::NO_CLICK:
			//Just move the mouse cursor iResult.mMouseMovement
			MoveMouse(iResult.mMouseMovement);
		break;
		default:
			std::cout << "Error in switch" << std::endl;
		break;
	}
}

bool getFingerData(std::vector<Idx_type>& oFoundGeoNodeIndices, 
				   std::vector<Vec3_type>& oFoundGeoNodePositions, 
				   PXCGesture *iGesture, 
				   const Idx_type iNumberOfNodesBothHands){

	typedef PXCGesture::GeoNode GeoNode;
	const auto numberOfNodesSingleHand = iNumberOfNodesBothHands/2;
	HandMouseResult result;

	//So no closed fist so check to find the position of the fingers and create a new mouse translation.
	std::vector<GeoNode> nodeInfo(iNumberOfNodesBothHands);

	bool fingerDetectSuccess = false;
	//If we don't detect at least one hand then we return false so that we can try again.
	fingerDetectSuccess |= iGesture->QueryNodeData(0, 
		GeoNode::LABEL_BODY_HAND_PRIMARY, 
		numberOfNodesSingleHand, 
		&nodeInfo[0]) >= PXC_STATUS_NO_ERROR;
		
	fingerDetectSuccess |= iGesture->QueryNodeData(0, 
		GeoNode::LABEL_BODY_HAND_SECONDARY, 
		numberOfNodesSingleHand, 
		&nodeInfo[numberOfNodesSingleHand]) >= PXC_STATUS_NO_ERROR;

	if(!fingerDetectSuccess)
		return false;

	for(size_t k=0; k<2; ++k){
		//We only want the fingers (i.e. only 5 of the nodes). 
		//We start at one because we don't want the palm.
		size_t j = 10*k + 1;
		size_t lastFingerIndex = 10*k + 6;
		for(; j<lastFingerIndex; ++j){
			Vec3_type fingerPos;
			for(int i=0; i<3; ++i)	
				fingerPos[i] = (&nodeInfo[j].positionWorld.x)[i];

			if(fingerPos[0] == (Scalar)0.0 && fingerPos[1] == (Scalar)0.0 && fingerPos[2] == (Scalar)0.0)
				continue;

			oFoundGeoNodeIndices.push_back(j); 
			oFoundGeoNodePositions.push_back(fingerPos);
		}
	}
	return oFoundGeoNodePositions.size() > 0;
}

void orderCornerPoints(std::array< Vec2i_type, 4>& ioCornerPoints){
	Vec2iXSorter sorter;
	std::sort(ioCornerPoints.begin(), ioCornerPoints.end(), sorter);
	//Swap the corner points so that I don't cover them
	if(ioCornerPoints[3][1] > ioCornerPoints[2][1])
		std::swap(ioCornerPoints[3], ioCornerPoints[2]);
	if(ioCornerPoints[0][1] > ioCornerPoints[1][1])
		std::swap(ioCornerPoints[0], ioCornerPoints[1]);

}
} //namespace easymotion



