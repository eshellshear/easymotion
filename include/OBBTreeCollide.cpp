#include "OBBTreeCollide.h"
#include "GeometricUtilityFunctions.h"

namespace easymotion{

void collideRecurse(collideResult& oResult, const OBBTree& iTree, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation, const int iNodeIndex){

	//Test for collision if not colliding then break.
	const auto& currentNode = iTree.getOBBNode(iNodeIndex);
	if(!collide(currentNode, iOBBHalfDepth, iFinger, iTreeTransformation))
		return;

	//std::cout << "Colliding with node " << iNodeIndex << std::endl;

	//Is the current node a leaf?
	if(currentNode.isLeaf()){
		oResult.mColliding = true;
		oResult.mCollidingKeys.push_back(currentNode.getKeyIndex());
		return;
	}

	//Else traverse further until we reach a leaf.
	collideRecurse(oResult, iTree, iOBBHalfDepth, iFinger, iTreeTransformation, currentNode.getChild1());

	collideRecurse(oResult, iTree, iOBBHalfDepth, iFinger, iTreeTransformation, currentNode.getChild2());
}

void obbTreeCollide(collideResult& oResult, const OBBTree& iTree, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation){
	//Begin the recursion
	oResult.init();
	collideRecurse(oResult, iTree, iOBBHalfDepth, iFinger, iTreeTransformation, 0);

}

} //namespace easymotion