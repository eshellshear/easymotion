#include "OBBTree.h"
#include "OBBTreeUtilities.h"
#include <cassert>

namespace easymotion{

OBBTree::OBBTree() {

}
OBBTree::OBBTree(const OBBTree& iOtherTree) {
	mNodes = iOtherTree.getNodes();
}

OBBTree& OBBTree::operator=(const OBBTree& iOtherTree){
		if (this == &iOtherTree)      // Same object?
			return *this; 
		//Set values
		mNodes = iOtherTree.getNodes();
		return *this;
}
bool OBBTree::buildTree(const Indexed_keys_vector& iIndexedKeys){
	//Allocate memory for the set of nodes
	mNodes.resize(2*iIndexedKeys.size()-1);
	//Create a copy of the iKeys vector that we can manipulate.
	Indexed_keys_vector indexedKeysCopy(iIndexedKeys);
	int nodeIndex = 0;
	buildRecurse(indexedKeysCopy, 0, indexedKeysCopy.size(), nodeIndex);
	return true;
}
void OBBTree::buildRecurse(Indexed_keys_vector& iIndexedKeys, const int iStart, const int iEnd, int& iNodeIndex){
	assert((size_t)iNodeIndex < mNodes.size());
	//So at each level I compute the smallest axis aligned box that fits around all the keys.
	//This defines the half width of the current node. I then find the axis of largest
	//variation and split along that axis. I sort the keys according to their middle points
	//and then swap them to the left or right dependning on whether they go to the left or 
	//right child of the current node. At the end we merely store the index to the key that defines the
	//leaf node.

	//Go through all iKeys and compute the max and min of the vertices defining their rotatedRectangles.
	Vec2_type keysMax, keysMin;
	keysMax[0] = -std::numeric_limits<Scalar>::max();
	keysMax[1] = -std::numeric_limits<Scalar>::max();
	keysMin[0] = std::numeric_limits<Scalar>::max();
	keysMin[1] = std::numeric_limits<Scalar>::max();

	computeKeysDimensions(keysMax, keysMin, iIndexedKeys, iStart, iEnd);
	const Vec2_type halfWidth((keysMax[0] - keysMin[0])*(Scalar)0.5, (keysMax[1] - keysMin[1])*(Scalar)0.5); 
	mNodes[iNodeIndex].setHW(halfWidth);
	const auto NKeysLeft = iEnd - iStart;
	if(NKeysLeft > 1){
		const Vec2_type offset((keysMax[0] + keysMin[0])*(Scalar)0.5, (keysMax[1] + keysMin[1])*(Scalar)0.5);
		mNodes[iNodeIndex].setOffset(offset);
		//Split all the keys between the two nodes.
		const int splitIndex = computeSplitIndexAndSwap(keysMax, keysMin, iIndexedKeys, iStart, iEnd);

		const auto currentIndex = iNodeIndex;
		++iNodeIndex;
		mNodes[currentIndex].setChild1(iNodeIndex);
		buildRecurse(iIndexedKeys, iStart, splitIndex, iNodeIndex);

		++iNodeIndex;
		mNodes[currentIndex].setChild2(iNodeIndex);
		buildRecurse(iIndexedKeys, splitIndex, iEnd, iNodeIndex);
	}else{
		//We have a leaf. No need to do ++iNodeIndex as it will be done after the recurse call if necessary.
		mNodes[iNodeIndex].setKey(iIndexedKeys[iStart].mIndex);
		mNodes[iNodeIndex].setOffset(iIndexedKeys[iStart].mKey.getOffset());
	}
}
bool OBBTree::isBuilt() const{
	return !mNodes.empty();
}

void OBBTree::setKeyIndex(const int iKeyIndex, const int iNodeIndex){
	assert(mNodes[iNodeIndex].isLeaf());
	mNodes[iNodeIndex].setKey(iKeyIndex);
}

const int OBBTree::getKeyIndex(const int iNodeIndex) const{
	return mNodes[iNodeIndex].getKeyIndex();
}
const OBBNode& OBBTree::getOBBNode(const int iNodeIndex) const{
	return mNodes[iNodeIndex];
}

const OBBTree::OBB_node_vector& OBBTree::getNodes() const{
	return mNodes;
}

}//namespace easymotion