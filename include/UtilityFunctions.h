#ifndef UTILITY_FUNCTIONS_H
#define UTILITY_FUNCTIONS_H

#include "BasicTypes.h"
#include <memory>
#include <array>
#include <boost/filesystem.hpp>

namespace fs=boost::filesystem;

class PXCGesture;
namespace easymotion{
/// Check whether the current file is a valid keyboard or not. True it is, false it isn't.
bool isValidKeyboard(const std::weak_ptr<FILE> iFile);
/// Count the number of valid keyboards located in the iDirectoryName directory above the executable directory. Return the files in the
/// oFiles variable and the number of them in the size_t parameter.
size_t enumerateKeyboards(std::vector<fs::path>& oFilePaths, const fs::path& iDirectoryName);
/// When a depth value is bad then we search for the closest depth value within a small radius of the current depth index.
bool getNewDepthValue(int& oCornerDepth, 
					  const unsigned char* iDepth_data_plane, 
					  const unsigned int iDepthWidth, 
					  const unsigned int iDepthHeight, 
					  const Vec2i_type& iDepthCornerPoint,
					  const std::array<float, 2>& iDepthBadValues);

/// Process the results of mouse movements for a Windows system.
void WINprocessResult(const HandMouseResult& iResult);

/// Collect finger info with the following function
bool getFingerData(std::vector<Idx_type>& oFoundGeoNodeIndices, 
				   std::vector<Vec3_type>& oFoundGeoNodePositions, 
				   PXCGesture *iGesture, 
				   const Idx_type iNumberOfNodesBothHands);

void orderCornerPoints(std::array< Vec2i_type, 4>& ioCornerPoints);
} // namespace easymotion



#endif