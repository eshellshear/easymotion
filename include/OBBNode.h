#ifndef OBBNODE_h
#define OBBNODE_h

#include <array>
#include "BasicTypes.h"

namespace easymotion{

//The node is defined by integers to children in the bounding volume array so that one is an INTMax when the node is a leaf. 
//Otherwise we're dealing with a normal node.
class OBBNode{
	typedef std::array<int, 2> IntPairArray;

public:
	OBBNode();
	OBBNode(const Vec2_type& iOffset, const IntPairArray iChildren);
	OBBNode(const Vec2_type& iOffset, const int iChild1, const int iChild2);
	OBBNode(const OBBNode& iOther);

	bool isLeaf() const;
	const int getChild1() const;
	const int getChild2() const;
	//If we are a leaf then child2 stores the key integer.
	const int getKeyIndex() const;
	const Vec2_type& getOffset() const;
	const IntPairArray& getChildren() const;
	const Vec2_type& getHW() const;

	void setKey(const int iKeyIndex);
	void setChild1(const int iChild);
	void setChild2(const int iChild);
	void setChildren(const int iChild1, const int iChild2);
	void setChildren(const IntPairArray& iChildren);
	void setOffset(const Vec2_type& iOffset);
	void setHW(const Vec2_type& iHW);

private:
	//We assume that all OBBs lie in the plane of the keyboard. The offset will be the 2d offset to the fiducial marker.
	//OBBs will be axis aligned w.r.t to the keyboard dimensions and regardless of the key orientations so no need for a Rotation.
	Vec2_type mOffset;
	Vec2_type mHalfWidths;
	//If mChildren[0] = std::numeric_limits<int>::max() then we have a leaf.
	IntPairArray mChildren;
};
}//namespace easymotion
#endif 