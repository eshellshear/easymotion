#include "GeometricUtilityFunctions.h"
#include <array>
#include <cmath>

#if defined (__SSE4_1__) || defined (__SSE4_2__) || defined(__AVX__)
#include <smmintrin.h>
#else
#include <xmmintrin.h>
#endif

using namespace std;

namespace easymotion{

bool OBBFingerCollisionTest(const Rotation& iOBBOrientation, const Vec3_type& iOBBCenter, const Vec3_type& iOBBHWs, const FingerObject& iFinger){
	__m128 zero = _mm_setzero_ps();
	__m128 center = _mm_loadu_ps(iFinger.mFingerPosition.val);
	__m128 ext = _mm_loadu_ps(iOBBHWs.val);
	__m128 mid = _mm_loadu_ps(iOBBCenter.val);
	__m128 xaxis = _mm_loadu_ps(iOBBOrientation.getCol(0).val);
	__m128 yaxis = _mm_loadu_ps(iOBBOrientation.getCol(1).val);
	__m128 zaxis = _mm_loadu_ps(iOBBOrientation.getCol(2).val);

	__m128 v = _mm_sub_ps(center, mid);

#if defined (__SSE4_1__) || defined (__SSE4_2__) || defined(__AVX__)
	__m128 xdot = _mm_dp_ps(v, xaxis, 0x73);
	__m128 ydot = _mm_dp_ps(v, yaxis, 0x73);
	__m128 zdot = _mm_dp_ps(v, zaxis, 0x73);
	__m128 d =_mm_shuffle_ps(xdot, ydot, _MM_SHUFFLE(0,1,0,1));
	d = _mm_shuffle_ps(d, zdot, _MM_SHUFFLE(1,0,2,0));
#else
	__m128 xmul = _mm_mul_ps(v, xaxis);
	__m128 ymul = _mm_mul_ps(v, yaxis);
	__m128 zmul = _mm_mul_ps(v, zaxis);

	// Re-arrange elements so we can use parallell adds
	__m128 d, d1, d2;
    __m128 tmp0, tmp2, tmp1, tmp3;
	tmp0   = _mm_shuffle_ps(xmul, ymul, 0x44);
	tmp2   = _mm_shuffle_ps(xmul, ymul, 0xEE);
	tmp1   = _mm_shuffle_ps(zmul, zero, 0x44);
	tmp3   = _mm_shuffle_ps(zmul, zero, 0xEE);
	d = _mm_shuffle_ps(tmp0, tmp1, 0x88);
	d1 = _mm_shuffle_ps(tmp0, tmp1, 0xDD);
	d2 = _mm_shuffle_ps(tmp2, tmp3, 0x88);
	d = _mm_add_ps(d, d1);
	d = _mm_add_ps(d, d2);
#endif
	__m128 e = _mm_add_ps(_mm_min_ps(_mm_add_ps(d, ext), zero), _mm_max_ps(_mm_sub_ps(d, ext), zero));
	e = _mm_mul_ps(e, e);
	
	const Vec3_type *p = (Vec3_type *) &e;
	const float r = iFinger.mFingerRadius;
	return ((*p)[0] + (*p)[1] + (*p)[2] <= r * r);
}

bool collide(const OBBNode& iNode, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation){

	//Create a rotated rectangle from the node and run the other function.
	RotatedRect rect;
	rect.angle = (Scalar)0.0;
	rect.center = iNode.getOffset();
	rect.size.width = (Scalar)2.0*iNode.getHW()[0];
	rect.size.height = (Scalar)2.0*iNode.getHW()[1];

	return collide(rect, iOBBHalfDepth, iFinger, iTreeTransformation);
}

void get3DPointFrom2d(Vec3_type& oPoint, const Vec3_type& iPoint, const Vec2_type& iOffSet, const Rotation& iRot){
	Vec3_type tmpPoint;
	tmpPoint[0] = iOffSet[0];
	tmpPoint[1] = iOffSet[1];
	tmpPoint[2] = 0.0;

	Vec3_type newPoint;
	rotationMult(newPoint, iRot, tmpPoint);

	oPoint = iPoint + newPoint;
}

void getRectOffset(Vec3_type& oOffset, const cv::Point2f& iRectCenter, const Rotation& iRot){
	const Vec2_type tmpCenter(iRectCenter.x, iRectCenter.y);
	getRectOffset(oOffset, tmpCenter, iRot);
}

void getRectOffset(Vec3_type& oOffset, const Vec2_type& iRectCenter, const Rotation& iRot){
	Vec3_type tmpOffset;
	tmpOffset[0] = iRectCenter[0];
	tmpOffset[1] = iRectCenter[1];
	tmpOffset[2] = (Scalar)0.0;
	
	rotationMult(oOffset, iRot, tmpOffset);
	//rotationInverseMult(oOffset, iRot, tmpOffset);
}

bool collide(const RotatedRect& iRect, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation){
	//First create an OBB from the RotatedRect data.
	Rotation OBBOrientation;
	if(iRect.angle == 0.0){
		OBBOrientation = iTreeTransformation.mR;
	}else{
		//Create a matrix based on the keys Rotation. The Rotation is around the z-axis so we create a matrix that looks like:
		//a b 0
		//c d 0
		//0 0 1
		Rotation tmpRot;
		getZAxisRot(tmpRot, iRect.angle);
		rotationMult(OBBOrientation, iTreeTransformation.mR, tmpRot);
	}

	Vec3_type offset;
	getRectOffset(offset, iRect.center, iTreeTransformation.mR);
	
	Vec3_type OBBHWs(iRect.size.width*(Scalar)0.5, iRect.size.height*(Scalar)0.5, iOBBHalfDepth);
	Vec3_type OBBCenter = iTreeTransformation.mT;
	OBBCenter += offset;

	//Now convert everything to Intel's coordinates etc.
	//OBBCenter = convertToIntel(OBBCenter);
	//OBBHWs = convertToIntel(OBBHWs);
	//OBBOrientation = convertToIntel(OBBOrientation);
	FingerObject correctedFinger(iFinger);
	correctedFinger.mFingerPosition = convertToNormal(correctedFinger.mFingerPosition);
	
	//Now do collision test and return the result.
	return OBBFingerCollisionTest(OBBOrientation, OBBCenter, OBBHWs, correctedFinger);
}

void computeNewRectAxes(Vec2_type& oWidthAxis, Vec2_type& oHeightAxis, const float iAngle){
	Vec2_type rotationMatrix[2];
	rotationMatrix[0][0] = cos(iAngle);
	rotationMatrix[0][1] = -sin(iAngle);
	rotationMatrix[1][0] = -rotationMatrix[0][1];
	rotationMatrix[1][1] = rotationMatrix[0][0];

	const Vec2_type XAxis(1.0,0.0), YAxis(0.0, 1.0);
	oWidthAxis = rotationMatrix[0].mul(XAxis) + rotationMatrix[0].mul(YAxis);
	oHeightAxis = rotationMatrix[1].mul(XAxis) + rotationMatrix[1].mul(YAxis);
}

Vec2_type getRotatedRectVertex(const RotatedRect& iRotatedRect, const int iVertIndex){
	assert(iVertIndex < 4);
	//Our rectangle vertices are numbered as follows:
	// 0-1  
	// | |
	// 2-3

	//Take care of the axis aligned case separately.
	Vec2_type vertex;
	if(iRotatedRect.angle == 0.0f){
		vertex[0] = iRotatedRect.center.x + (1.0f - 2.0f*(float)(iVertIndex & 1))*iRotatedRect.size.width*0.5f;
		vertex[1] = iRotatedRect.center.y + (1.0f - 2.0f*(float)((iVertIndex >> 1) & 1))*iRotatedRect.size.height*0.5f;

		return vertex;
	}

	//Now compute each of the vertices in the same way as above but for the rotated rectangle case. First we compute the Rotation. 
	Vec2_type newWidthAxis, newHeightAxis;
	computeNewRectAxes(newWidthAxis, newHeightAxis, iRotatedRect.angle);
	const auto tmpXVec2 = newWidthAxis*(1.0f - 2.0f*(float)(iVertIndex & 1))*iRotatedRect.size.width*0.5f;
	assert(length(tmpXVec2) == iRotatedRect.size.width*0.5f);
	vertex = add2d(iRotatedRect.center, tmpXVec2);
	const auto tmpYVec2 = newHeightAxis*(1.0f - 2.0f*(float)((iVertIndex >> 1) & 1))*iRotatedRect.size.height*0.5f;
	vertex += tmpYVec2;

	return vertex;
}

Scalar distance2ToKey(const Key& iKey, const FingerObject& iFinger, const Transformation iTransformation){
	Vec3_type keyPosition = iTransformation.mT;
	Vec3_type offset;
	getRectOffset(offset, iKey.getOffset(), iTransformation.mR);
	keyPosition += offset;

	//There's actually no need to subtract the finger's radius because it's the same for all keys.
	//In addition, I already know that the finger is colliding with this key.
	//Scalar dist = distance(keyPosition, iFinger.mFingerPosition);
	//return std::max(dist - iFinger.mFingerRadius, (Scalar)0.0);
	return distance2(keyPosition, iFinger.mFingerPosition);
}

Scalar distanceToKey(const Key& iKey, const FingerObject& iFinger, const Transformation iTransformation){
	return sqrtf(distance2ToKey(iKey, iFinger, iTransformation));
}

void rectangleInteriorPoints(std::vector< Vec2i_type >& oInteriorPoints, const std::array< Vec2i_type, 4>& iCornerPoints){
	//Just collect all points in the smallest rectangle containing iCornerPoints 
	auto left = std::min(iCornerPoints[0][0], std::min(iCornerPoints[1][0], std::min(iCornerPoints[2][0], iCornerPoints[3][0])));
    auto right = std::max(iCornerPoints[0][0], std::max(iCornerPoints[1][0], std::max(iCornerPoints[2][0], iCornerPoints[3][0])));

    auto bottom = std::min(iCornerPoints[0][1], std::min(iCornerPoints[1][1], std::min(iCornerPoints[2][1], iCornerPoints[3][1])));
	auto top = std::max(iCornerPoints[0][1], std::max(iCornerPoints[1][1], std::max(iCornerPoints[2][1], iCornerPoints[3][1])));

	//Increase size of all corner points by 25% so that we make sure we get the whole piece of paper
	const int fractionExtra = 4;
	const int extraWidth = (right - left)/fractionExtra;
	const int extraHeight = (top - bottom)/fractionExtra;

	left -= extraWidth;
	right += extraWidth;
	top += extraHeight;
	bottom -= extraHeight;

	oInteriorPoints.resize((top-bottom+1)*(right-left+1));
    Idx_type i=0;

	for(int YCoord = bottom; YCoord <= top; ++YCoord)
		for(int  XCoord = left; XCoord <= right; ++XCoord, ++i)
            oInteriorPoints[i] = Vec2i_type(XCoord, YCoord);

}

void getPlaneNormalAndScalar(Vec3_type& oPlaneNormal, Scalar& oPlaneValue, const std::array< Vec2i_type, 4>& iCornerPoints, const std::array<int, 4>& iDepths){
	std::array<Vec3_type, 3> planePoints;
	for(Idx_type i=0;i<3;++i){
		planePoints[i][0] = (Scalar)iCornerPoints[i][0];
		planePoints[i][1] = (Scalar)iCornerPoints[i][1];
		planePoints[i][2] = (Scalar)iDepths[i];
	}

	std::array<Vec3_type, 2> planeSides;
	planeSides[0] = planePoints[1] - planePoints[0];
	planeSides[1] = planePoints[2] - planePoints[0];

	oPlaneNormal = crossProd(planeSides[0], planeSides[1]);
	oPlaneNormal = normalizeVector(oPlaneNormal);

	oPlaneValue = dotProd(oPlaneNormal, planePoints[0]);
}

int bilinearInterpolationFloor(const std::array<Vec2i_type, 4>& iCornerPoints, const std::array<int, 4>& iDepths, const Vec2i_type& iPoint){
	//We assume the points are as in the above algorithm. So we want to interpolate between 0 and 1 and then 2 and 3. 
	//Note: we cannot have that points 0 and 1 are across the diagonal from each other.
	
	auto dist1 = pointDistance(iCornerPoints[0], iCornerPoints[1]);
	auto dist2 = pointDistance(iCornerPoints[2], iCornerPoints[3]);

	Vec2_type projectedPoints[2];
	projectPoint(projectedPoints[0], iCornerPoints[0], iCornerPoints[1], iPoint);
	projectPoint(projectedPoints[1], iCornerPoints[2], iCornerPoints[3], iPoint);

	auto linearInterpol1 = (pointDistance(projectedPoints[0], iCornerPoints[0])/dist1)*iDepths[0] + (pointDistance(projectedPoints[0], iCornerPoints[1])/dist1)*iDepths[1];

	auto linearInterpol2 = (pointDistance(projectedPoints[1], iCornerPoints[2])/dist2)*iDepths[2] + (pointDistance(projectedPoints[1], iCornerPoints[3])/dist2)*iDepths[3];

	auto dist3 = pointDistance(projectedPoints[0], projectedPoints[1]);

	auto bilinearInterpolDepth = (pointDistance(projectedPoints[0], iPoint)/dist3)*linearInterpol1 + (pointDistance(projectedPoints[1], iPoint)/dist3)*linearInterpol2;

	return (int)std::floor(bilinearInterpolDepth);

}

Rotation convertToIntel(const Rotation& iRotation){
	Rotation intelRotation;
	
	easymotion::Rotation alvarToIntelRotation;
	for(int i=0; i<3;++i)
		for(int j=0; j<3;++j)
			alvarToIntelRotation[i][j] = 0.0;

	alvarToIntelRotation[0][0] = 1.0;
	alvarToIntelRotation[1][2] = 1.0;
	alvarToIntelRotation[2][1] = 1.0;

	rotationMult(intelRotation, iRotation, alvarToIntelRotation);

	return intelRotation;
}

Vec3_type convertToIntel(const Vec3_type& iVector){
	Vec3_type intelVector;
	
	//Convert alvar units to meters.
	const Scalar metersToCentimeters = (Scalar)0.01;
	for(Idx_type i=0;i<3;++i)
		intelVector[i] = metersToCentimeters*iVector[i]; 
	

	easymotion::Rotation alvarToIntelRotation;
	for(int i=0; i<3;++i)
		for(int j=0; j<3;++j)
			alvarToIntelRotation[i][j] = 0.0;

	alvarToIntelRotation[0][0] = 1.0;
	alvarToIntelRotation[1][2] = 1.0;
	alvarToIntelRotation[2][1] = 1.0;

	Vec3_type dummyTranslation(intelVector);
	rotationMult(intelVector, alvarToIntelRotation, dummyTranslation);

	return intelVector;

}

Vec3_type convertToNormal(const Vec3_type& iVector){
	Vec3_type normalVector;
	
	//Convert alvar units to centimetres.
	const Scalar centimetersToMeters = (Scalar)100.0;
	for(Idx_type i=0;i<3;++i)
		normalVector[i] = centimetersToMeters*iVector[i]; 
	

	easymotion::Rotation alvarToIntelRotation;
	for(int i=0; i<3;++i)
		for(int j=0; j<3;++j)
			alvarToIntelRotation[i][j] = 0.0;

	alvarToIntelRotation[0][0] = 1.0;
	alvarToIntelRotation[1][2] = 1.0;
	alvarToIntelRotation[2][1] = 1.0;

	Vec3_type dummyTranslation(normalVector);
	rotationMult(normalVector, alvarToIntelRotation, dummyTranslation);

	return normalVector;
}

}//namespace easymotion