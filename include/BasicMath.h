#ifndef BASIC_MATH_H
#define BASIC_MATH_H

#include "BasicTypes.h"
#include <Pose.h>
#include <opencv2/core/core.hpp>
#include <cassert>
#include <cmath>

namespace easymotion{

/*****
 Functions to manipulate vectors and rotations, etc.
*****/

inline Scalar length(const Vec2_type& iVec){
	return sqrtf(iVec[0]*iVec[0] + iVec[1]*iVec[1]);
}

inline Scalar length(const Vec3_type& iVec){
	return sqrtf(iVec[0]*iVec[0] + iVec[1]*iVec[1] + iVec[2]*iVec[2]);
}

inline int dotProd(const Vec2i_type& iVec1, const Vec2i_type& iVec2){
	return iVec1[0]*iVec2[0]+iVec1[1]*iVec2[1];
}

inline Scalar dotProd(const Vec2_type& iVec1, const Vec2_type& iVec2){
	return iVec1[0]*iVec2[0]+iVec1[1]*iVec2[1];
}

inline Scalar dotProd(const Vec3_type& iVec1, const Vec3_type& iVec2){
	return iVec1[0]*iVec2[0]+iVec1[1]*iVec2[1]+iVec1[2]*iVec2[2];
}

inline Vec3_type crossProd(const Vec3_type& iVec1, const Vec3_type& iVec2){
	return Vec3_type(iVec1[1]*iVec2[2] - iVec1[2]*iVec2[1], iVec1[2]*iVec2[0] - iVec1[0]*iVec2[2], iVec1[0]*iVec2[1] - iVec1[1]*iVec2[0]);
}

inline Vec3_type normalizeVector(const Vec3_type& iVec){
	const Scalar invVectorLength = (Scalar)1.0/length(iVec);
	return Vec3_type(iVec[0]*invVectorLength, iVec[1]*invVectorLength, iVec[2]*invVectorLength);
}

template<class TScalar>
inline TScalar sqr2(const TScalar iVal) { return iVal*iVal;}

template<class TVector1, class TVector2>
inline float pointDistance(const TVector1& iP, const TVector2& iQ){
	return sqrtf(sqr2((float)iP[0] - (float)iQ[0]) + sqr2((float)iP[1] - (float)iQ[1]));
}

inline Scalar distance(const Vec3_type& iVec1, const Vec3_type& iVec2){
	return sqrtf(sqr2(iVec1[0] - iVec2[0]) + sqr2(iVec1[1] - iVec2[1]) + sqr2(iVec1[2] - iVec2[2]));
}

inline Scalar distance2(const Vec3_type& iVec1, const Vec3_type& iVec2){
	return sqr2(iVec1[0] - iVec2[0]) + sqr2(iVec1[1] - iVec2[1]) + sqr2(iVec1[2] - iVec2[2]);
}

/// To go between types
template<class TVecType>
inline TVecType add2d(const cv::Point2f& iVec1, const TVecType& iVec2){
	TVecType tmpVec;
	tmpVec[0] = iVec1.x + iVec2[0];
	tmpVec[1] = iVec1.y + iVec2[1];
	return tmpVec;
}
/// Compute the 3D Rotation around the z-axis through an angle of iAngle.
template<class TRotType>
inline void getZAxisRot(TRotType& oR, const Scalar iAngle){
	oR[0][0] = cos(iAngle);
	oR[0][1] = -sin(iAngle);
	oR[0][2] = 0.0;

	oR[1][0] = -oR[0][1];
	oR[1][1] = oR[0][0];
	oR[1][2] = 0.0;

	oR[2][0] = 0.0;
	oR[2][1] = 0.0;
	oR[2][2] = 1.0;
}

/// Multiply two Rotation matrices. Output Rotation type same as first type.
template<class TRotType1, class TRotType2>
inline void rotationMult(TRotType1& oR, const TRotType1& iR1, const TRotType2& iR2){
	assert(&oR != &iR1);
    assert(&oR != &iR2);
    oR[0][0] = iR1[0][0] * iR2[0][0] + iR1[0][1] * iR2[1][0] + iR1[0][2] * iR2[2][0];
    oR[1][0] = iR1[1][0] * iR2[0][0] + iR1[1][1] * iR2[1][0] + iR1[1][2] * iR2[2][0];
    oR[2][0] = iR1[2][0] * iR2[0][0] + iR1[2][1] * iR2[1][0] + iR1[2][2] * iR2[2][0];
    oR[0][1] = iR1[0][0] * iR2[0][1] + iR1[0][1] * iR2[1][1] + iR1[0][2] * iR2[2][1];
    oR[1][1] = iR1[1][0] * iR2[0][1] + iR1[1][1] * iR2[1][1] + iR1[1][2] * iR2[2][1];
    oR[2][1] = iR1[2][0] * iR2[0][1] + iR1[2][1] * iR2[1][1] + iR1[2][2] * iR2[2][1];
    oR[0][2] = iR1[0][0] * iR2[0][2] + iR1[0][1] * iR2[1][2] + iR1[0][2] * iR2[2][2];
    oR[1][2] = iR1[1][0] * iR2[0][2] + iR1[1][1] * iR2[1][2] + iR1[1][2] * iR2[2][2];
    oR[2][2] = iR1[2][0] * iR2[0][2] + iR1[2][1] * iR2[1][2] + iR1[2][2] * iR2[2][2];
}
/// Multiply by inverse of second Rotation. Output Rotation type same as first type.
template<class TRotType1, class TRotType2>
inline void rotationInverseMult(TRotType1& oR, const TRotType1& iR1, const TRotType2& iR2){
	assert(&oR != &iR1);
    assert(&oR != &iR2);
    oR[0][0] = iR1[0][0] * iR2[0][0] + iR1[0][1] * iR2[0][1] + iR1[0][2] * iR2[0][2];
    oR[1][0] = iR1[1][0] * iR2[0][0] + iR1[1][1] * iR2[0][1] + iR1[1][2] * iR2[0][2];
    oR[2][0] = iR1[2][0] * iR2[0][0] + iR1[2][1] * iR2[0][1] + iR1[2][2] * iR2[0][2];
    oR[0][1] = iR1[0][0] * iR2[1][0] + iR1[0][1] * iR2[1][1] + iR1[0][2] * iR2[1][2];
    oR[1][1] = iR1[1][0] * iR2[1][0] + iR1[1][1] * iR2[1][1] + iR1[1][2] * iR2[1][2];
    oR[2][1] = iR1[2][0] * iR2[1][0] + iR1[2][1] * iR2[1][1] + iR1[2][2] * iR2[1][2];
    oR[0][2] = iR1[0][0] * iR2[2][0] + iR1[0][1] * iR2[2][1] + iR1[0][2] * iR2[2][2];
    oR[1][2] = iR1[1][0] * iR2[2][0] + iR1[1][1] * iR2[2][1] + iR1[1][2] * iR2[2][2];
    oR[2][2] = iR1[2][0] * iR2[2][0] + iR1[2][1] * iR2[2][1] + iR1[2][2] * iR2[2][2];
}
/// Multiply a vector by a Rotation matrix.
template<class TRotType>
inline void rotationMult(Vec3_type& oV, const TRotType& iR, const Vec3_type& iV){
	assert(&oV != &iV);
	oV[0] = iR[0][0] * iV[0] + iR[0][1] * iV[1] + iR[0][2] * iV[2];
    oV[1] = iR[1][0] * iV[0] + iR[1][1] * iV[1] + iR[1][2] * iV[2];
    oV[2] = iR[2][0] * iV[0] + iR[2][1] * iV[1] + iR[2][2] * iV[2];
}
/// Multiply a vector by the inverse of a Rotation matrix.
template<class TRotType>
inline void rotationInverseMult(Vec3_type& oV, const TRotType& iR, const Vec3_type& iV){
	assert(&oV != &iV);
	oV[0] = iR[0][0] * iV[0] + iR[1][0] * iV[1] + iR[2][0] * iV[2];
    oV[1] = iR[0][1] * iV[0] + iR[1][1] * iV[1] + iR[2][1] * iV[2];
    oV[2] = iR[0][2] * iV[0] + iR[1][2] * iV[1] + iR[2][2] * iV[2];
}
/// Get inverse of a Rotation matrix
template<class TRotType>
inline void rotationInverse(TRotType& oR, const TRotType& iR){
	const float R01=iR[0][1], R02=iR[0][2], R12=iR[1][2];
    oR[0][0] = iR[0][0];    
	oR[0][1] = iR[1][0];    
	oR[0][2] = iR[2][0];       
    oR[1][0] = R01;        
	oR[1][1] = iR[1][1];    
	oR[1][2] = iR[2][1];
    oR[2][0] = R02;        
	oR[2][1] = R12;        
	oR[2][2] = iR[2][2];
}
template<class TTransf>
inline void transform(Vec3_type& oV, const TTransf& iT, const Vec3_type& iV){
	rotationMult(oV, iT.mR, iV);
	oV += iT.mT;
}
inline void Rot2cvRot(CvMat* oR, const Rotation& iRot){
	cvmSet(oR, 0, 0, iRot[0][0]);
	cvmSet(oR, 0, 1, iRot[0][1]);
	cvmSet(oR, 0, 2, iRot[0][2]);

	cvmSet(oR, 1, 0, iRot[1][0]);
	cvmSet(oR, 1, 1, iRot[1][1]);
	cvmSet(oR, 1, 2, iRot[1][2]);

	cvmSet(oR, 2, 0, iRot[2][0]);
	cvmSet(oR, 2, 1, iRot[2][1]);
	cvmSet(oR, 2, 2, iRot[2][2]);
}

inline void cvRot2Rot(Rotation& oR, const CvMat* iRotMat){
	oR[0][0] = (Scalar)cvmGet(iRotMat, 0, 0);
	oR[0][1] = (Scalar)cvmGet(iRotMat, 0, 1);
	oR[0][2] = (Scalar)cvmGet(iRotMat, 0, 2);

	oR[1][0] = (Scalar)cvmGet(iRotMat, 1, 0);
	oR[1][1] = (Scalar)cvmGet(iRotMat, 1, 1);
	oR[1][2] = (Scalar)cvmGet(iRotMat, 1, 2);

	oR[2][0] = (Scalar)cvmGet(iRotMat, 2, 0);
	oR[2][1] = (Scalar)cvmGet(iRotMat, 2, 1);
	oR[2][2] = (Scalar)cvmGet(iRotMat, 2, 2);

}

inline void getCvKeyRot(CvMat* oR, const CvMat* iOrigCvMat, const Scalar iAngle){
	Rotation keyRotation, keyRot, origRot;
	getZAxisRot(keyRotation, iAngle);

	cvRot2Rot(origRot, iOrigCvMat);
	rotationMult(keyRot, origRot, keyRotation);
	Rot2cvRot(oR, keyRot);
}

inline Transformation getTransfFromPose(const alvar::Pose& p){
	Transformation tmpTransf;
	tmpTransf.mT[0] = (Scalar)p.translation[0];
	tmpTransf.mT[1] = (Scalar)p.translation[1];
	tmpTransf.mT[2] = (Scalar)p.translation[2];

	double quat[4];
	p.GetQuaternion(quat);
	double m[9];
	const double localEpsilon = 1e-6;
	assert(abs(quat[0]*quat[0] + quat[1]*quat[1] + quat[2]*quat[2] + quat[3]*quat[3] - 1.0) < localEpsilon);
	p.QuatToMat9(quat, m);
	//Each column of the transformation defines an orientation axis 
	tmpTransf.mR[0][0] = (Scalar)m[0];
	tmpTransf.mR[0][1] = (Scalar)m[1];
	tmpTransf.mR[0][2] = (Scalar)m[2];
	
	tmpTransf.mR[1][0] = (Scalar)m[3];
	tmpTransf.mR[1][1] = (Scalar)m[4];
	tmpTransf.mR[1][2] = (Scalar)m[5];

	tmpTransf.mR[2][0] = (Scalar)m[6];
	tmpTransf.mR[2][1] = (Scalar)m[7];
	tmpTransf.mR[2][2] = (Scalar)m[8];

	return tmpTransf;
}

} //namespace easymotion

#endif