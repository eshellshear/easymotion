#ifndef OBBTREECOLLIDE_h
#define OBBTREECOLLIDE_h

#include "BasicTypes.h"
#include "OBBTree.h"

namespace easymotion{

struct collideResult{
	collideResult() : mColliding(false) {}

	void init(){
		mColliding=false;
		mCollidingKeys.clear();
	}

	bool mColliding;
	Int_vector mCollidingKeys;
};


/// This function collides an object with the tree.
void obbTreeCollide(collideResult& oResult, const OBBTree& iTree, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation);
/// The recursive function call to find out which keys are colliding with the finger.
void collideRecurse(collideResult& oResult, const OBBTree& iTree, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation, const int iNodeIndex);

} //namespace easymotion


#endif