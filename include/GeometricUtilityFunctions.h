#ifndef GEOMETRIC_UTILITY_FUNCTIONS_H
#define GEOMETRIC_UTILITY_FUNCTIONS_H

#include "BasicTypes.h"
#include "BasicMath.h"
#include "OBBNode.h"
#include <cassert>

namespace easymotion{

/****
* General geometric queries.
****/

/// Compute the 3D location of a point given a 2D offset and a 3D rotation matrix. We assume the first 2 columns
/// of the matrix are the axes.
void get3DPointFrom2d(Vec3_type& oPoint, const Vec3_type& iPoint, const Vec2_type& iOffSet, const Rotation& iRot);
/// Determine whether a FingerObject and an OBB are colliding.
bool OBBFingerCollisionTest(const Rotation& iOBBOrientation, const Vec3_type& iOBBCenter, const Vec3_type& iOBBHWs, const FingerObject& iFinger);
/// Small helper function to get the 3D offsets from the fiducial marker
void getRectOffset(Vec3_type& oOffset, const cv::Point2f& iRectCenter, const Rotation& iRot);
void getRectOffset(Vec3_type& oOffset, const Vec2_type& iRectCenter, const Rotation& iRot);
/// Compute whether a RotatedRect and a FingerObject are colliding
bool collide(const RotatedRect& iRect, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation);
/// Compute whether a finger object and an OBBNode are colliding.
bool collide(const OBBNode& iNode, const Scalar iOBBHalfDepth, const FingerObject& iFinger, const Transformation& iTreeTransformation);
/// Get the coordinates of iVertIndex in iRotatedRect. iVectIndex = 0,1,2,3.
Vec2_type getRotatedRectVertex(const RotatedRect& iRotatedRect, const int iVertIndex);
/// Compute the new Vec2_type axes (originally (1,0) and (0,1)) after rotating the rectangle thru angle iAngle.
void computeNewRectAxes(Vec2_type& iWidthAxis, Vec2_type& iHeightAxis, const float iAngle);
/// Compute the minimum distance between the centrepoint of the key and the finger object. distance2ToKey is the
/// squared distance.
Scalar distance2ToKey(const Key& iKey, const FingerObject& iFinger, const Transformation iTransformation);
Scalar distanceToKey(const Key& iKey, const FingerObject& iFinger, const Transformation iTransformation);

/// Find all the depth coordinates of a set of points defined by the boundary of 4 points in the depth stream
void rectangleInteriorPoints(std::vector< Vec2i_type >& oInteriorPoints, const std::array< Vec2i_type, 4>& iCornerPoints);

/// Get the height of a 2D point above the plane.
inline Scalar getPlaneDepth(const Vec3_type& iPlaneNormal, const Scalar iPlaneValue, Vec2i_type iPoint){
	return (iPlaneValue - iPlaneNormal[0]*(Scalar)iPoint[0] - iPlaneNormal[1]*(Scalar)iPoint[1])/iPlaneNormal[2];
}

/// Find the distance of a 3D point above a plane. The vector iPlaneNormal must be normalized!
inline Scalar distanceToPlane(const Vec3_type& iPlaneNormal, const Scalar iPlaneValue, const Vec3_type& iPoint){
	return dotProd(iPlaneNormal, iPoint) - iPlaneValue;
}

/// Compute the plane that goes through the points 
void getPlaneNormalAndScalar(Vec3_type& oPlaneNormal, Scalar& oPlaneValue, const std::array< Vec2i_type, 4>& iCornerPoints, const std::array<int, 4>& iDepths);

/// A function to project a point iP onto the line connecting iP1 to iP2. The point oProjectedPoint is the resulting point.
inline void projectPoint(Vec2_type& oProjectedPoint, const Vec2i_type& iP1, const Vec2i_type& iP2, const Vec2i_type& iP){
	//The normal projection to the closest points would be as in the algorithm below however we want to project to closest y-axis
	Vec2i_type gradient;
	gradient = iP2 - iP1;

	auto direction = iP - iP1;
	Scalar denom = (Scalar)dotProd(gradient, gradient);
	Scalar t = (Scalar)dotProd(direction, gradient)/ denom;

	t = std::min((Scalar)1.0, t);
	t = std::max((Scalar)0.0, t);

	oProjectedPoint[0] = t*gradient[0] + iP1[0];
	oProjectedPoint[1] = t*gradient[1] + iP1[1];
	//Vec2i_type lowerPoint(iP1), upperPoint(iP2);
	//if(iP1[1] > iP2[1]){ 
	//	lowerPoint = iP2;
	//	upperPoint = iP1;
	//}

	//auto denom = upperPoint[1] - lowerPoint[1];
	//if(denom == 0){
	//	oProjectedPoint = iP2;	
	//	return;
	//}

	//Scalar t = (Scalar)(iP[1] - lowerPoint[1])/(Scalar)denom;

	//t = std::min((Scalar)1.0, t);
	//t = std::max((Scalar)0.0, t);

	//Vec2i_type gradient;
	//gradient = upperPoint - lowerPoint;
	//oProjectedPoint[0] = t*(Scalar)gradient[0] + (Scalar)lowerPoint[0];
	//oProjectedPoint[1] = t*(Scalar)gradient[1] + (Scalar)lowerPoint[1];
}

//Bilinear interpolate the depth of iPoint given the corner points iCornerPoints with depths iDepths and return it as an int.
int bilinearInterpolationFloor(const std::array<Vec2i_type, 4>& iCornerPoints, const std::array<int, 4>& iDepths, const Vec2i_type& iPoint);

//Convert iVector in normal world coordinates (centimetres etc) to Intel coordinates and return it
Vec3_type convertToIntel(const Vec3_type& iVector);
//Convert iRotation with normal world axes to Intel (i.e. swap y and z) and return it
Rotation convertToIntel(const Rotation& iRotation);
//Convert iVector in Intel coordinates to normal world coordinates and return it
Vec3_type convertToNormal(const Vec3_type& iVector);

}//namespace easymotion
#endif