#include "KeyboardCreator.h"

using namespace std;
using namespace alvar;

namespace easymotion{

KeyboardCreator::KeyboardCreator() : mKeyboardImg(nullptr),
          prompt(false),
          units(96.0/2.54),      // cm assuming 96 dpi
          marker_side_len(9.0),  // 9 cm
          marker_type(0),
          posx(0), posy(0),
          content_res(0),        // 0 uses default
          margin_res(0.0),       // 0.0 uses default (can be n*0.5)
          marker_data_content_type(MarkerData::MARKER_CONTENT_TYPE_NUMBER),
          marker_data_force_strong_hamming(false) 
{

}

KeyboardCreator::~KeyboardCreator(){
	 if (mKeyboardImg) cvReleaseImage(&mKeyboardImg);
}

bool KeyboardCreator::createKeyboard(std::string& iKeyboardType){
	if(iKeyboardType.compare("English") != 0)
		return false;

	char markerId = 0;
	if(!createMarker(&markerId))
		return false;

	if(!createKeys())
		return false;

	if(!saveKeyboard())
		return false;

	return true;
}
bool KeyboardCreator::createMarker(const char *iId){
	//First of all create the marker
	 if (marker_type == 0) {
        MarkerData md(marker_side_len, content_res, margin_res);
        int side_len = int(marker_side_len*units+0.5);
        if (mKeyboardImg == nullptr) {
            mKeyboardImg = cvCreateImage(cvSize(side_len, side_len), IPL_DEPTH_8U, 1);
            filename.str("");
            filename<<"MarkerData";
            minx = (posx*units) - (marker_side_len*units/2.0);
            miny = (posy*units) - (marker_side_len*units/2.0);
            maxx = (posx*units) + (marker_side_len*units/2.0);
            maxy = (posy*units) + (marker_side_len*units/2.0);
        } else {
            double new_minx = (posx*units) - (marker_side_len*units/2.0);
            double new_miny = (posy*units) - (marker_side_len*units/2.0);
            double new_maxx = (posx*units) + (marker_side_len*units/2.0);
            double new_maxy = (posy*units) + (marker_side_len*units/2.0);
            if (minx < new_minx) new_minx = minx;
            if (miny < new_miny) new_miny = miny;
            if (maxx > new_maxx) new_maxx = maxx;
            if (maxy > new_maxy) new_maxy = maxy;
            IplImage *new_img = cvCreateImage(cvSize(int(new_maxx-new_minx+0.5), int(new_maxy-new_miny+0.5)), IPL_DEPTH_8U, 1);
            cvSet(new_img, cvScalar(255));
            CvRect roi = cvRect(int(minx-new_minx+0.5), int(miny-new_miny+0.5), mKeyboardImg->width, mKeyboardImg->height);
            cvSetImageROI(new_img, roi);
            cvCopy(mKeyboardImg, new_img);
            cvReleaseImage(&mKeyboardImg);
            mKeyboardImg = new_img;
            roi.x = int((posx*units) - (marker_side_len*units/2.0) - new_minx + 0.5); 
            roi.y = int((posy*units) - (marker_side_len*units/2.0) - new_miny + 0.5); 
            roi.width = int(marker_side_len*units+0.5); roi.height = int(marker_side_len*units+0.5);
            cvSetImageROI(mKeyboardImg, roi);
            minx = new_minx; miny = new_miny;
            maxx = new_maxx; maxy = new_maxy;
        }
        if (marker_data_content_type == MarkerData::MARKER_CONTENT_TYPE_NUMBER) {
            int idi = atoi(iId);
            md.SetContent(marker_data_content_type, idi, 0);
            if (filename.str().length()<64) filename<<"_"<<idi;

            Pose pose;
            pose.Reset();
            pose.SetTranslation(posx, -posy, 0);
            multi_marker.PointCloudAdd(idi, marker_side_len, pose);
        } else {
            md.SetContent(marker_data_content_type, 0, iId);
            const char *p = iId;
            int counter=0;
            filename<<"_";
            while(*p) {
                if (!isalnum(*p)) filename<<"_";
                else filename<<(char)tolower(*p);
                p++; counter++;
                if (counter > 8) break;
            }
        }
        md.ScaleMarkerToImage(mKeyboardImg);
        cvResetImageROI(mKeyboardImg);
    }
    else if (marker_type == 1) {
        // Create and save MarkerArtoolkit marker (Note, that this doesn't support multi markers)
        MarkerArtoolkit md(marker_side_len, content_res, margin_res);
        int side_len = int(marker_side_len*units+0.5);
        if (mKeyboardImg != 0) cvReleaseImage(&mKeyboardImg);
        mKeyboardImg = cvCreateImage(cvSize(side_len, side_len), IPL_DEPTH_8U, 1);
        filename.str("");
        filename<<"MarkerArtoolkit";
        md.SetContent(atoi(iId));
        filename<<"_"<<atoi(iId);
        md.ScaleMarkerToImage(mKeyboardImg);
    }
	return true;
}
bool KeyboardCreator::createKeys(){
	//We should have already created the marker.
	if(!mKeyboardImg)
		return false;



	return true;
}
bool KeyboardCreator::saveKeyboard(){
	//Save marker and keyboard together to a file.
	if(!mKeyboardImg)
		return false;

    std::stringstream filenamexml;
    filenamexml<<filename.str()<<".xml";
    filename<<".png";
    std::cout<<"Saving: "<<filename.str()<<std::endl;
    cvSaveImage(filename.str().c_str(), mKeyboardImg);
	
	//Should only have a size of 1.
	assert(multi_marker.Size() == 1);
    return true;
}

}//namespace easymotion