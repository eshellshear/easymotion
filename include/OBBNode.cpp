#include "OBBNode.h"
#include <cassert>
#include <limits>

namespace easymotion{

OBBNode::OBBNode(){
	setChild1(-invalid_int);
	setChild2(-invalid_int);
}
OBBNode::OBBNode(const Vec2_type& iOffset, const IntPairArray iChildren) : mOffset(iOffset), mChildren(iChildren) {
}

OBBNode::OBBNode(const Vec2_type& iOffset, const int iChild1, const int iChild2) : mOffset(iOffset) {
	setChild1(iChild1);
	setChild2(iChild2);
}
OBBNode::OBBNode(const OBBNode& iOther) {
	setOffset(iOther.getOffset());
	setChildren(iOther.getChildren());
}

bool OBBNode::isLeaf() const{
	return mChildren[0] == invalid_int;
}
const int OBBNode::getChild1() const{
	return mChildren[0];
}
const int OBBNode::getChild2() const{
	return mChildren[1];
}
const int OBBNode::getKeyIndex() const{
	//TODO: only use assert if in debug mode.
	assert(mChildren[0] == invalid_int);
	return mChildren[1];
}
const Vec2_type& OBBNode::getOffset() const {
	return mOffset;
}
const OBBNode::IntPairArray& OBBNode::getChildren() const {
	return mChildren;
}
const Vec2_type& OBBNode::getHW() const {
	return mHalfWidths;
}

void OBBNode::setKey(const int iKeyIndex){
	setChild1(invalid_int);
	setChild2(iKeyIndex);
}
void OBBNode::setChild1(const int iChild){
	mChildren[0] = iChild;
}
void OBBNode::setChild2(const int iChild){
	mChildren[1] = iChild;
}
void OBBNode::setChildren(const int iChild1, const int iChild2){
	setChild1(iChild1);
	setChild2(iChild2);
}
void OBBNode::setChildren(const IntPairArray& iChildren){
	mChildren = iChildren;
}
void OBBNode::setOffset(const Vec2_type& iOffset){
	mOffset = iOffset;
}
void OBBNode::setHW(const Vec2_type& iHW){
	mHalfWidths = iHW;
}
}//namespace easymotion