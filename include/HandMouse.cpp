#include "HandMouse.h"
#include "BasicMath.h"
#include "UtilityFunctions.h"

#include <pxcgesture.h>

//using std::chrono::steady_clock;
//using std::chrono::duration_cast;
//using std::chrono::milliseconds;

namespace easymotion{
	
HandMouse::HandMouse() :mLastGeoNodeTracked(invalid_idx), mLastPosition(0.0,0.0,0.0), mNFingersTracked(5) {
		
	//mLastClosedHandTime = std::chrono::steady_clock::time_point::min();
}

HandMouseResult HandMouse::processUserFingers(PXCGesture *iGesture, const Idx_type iNumberOfNodesBothHands){
	
	HandMouseResult result;
	//Is the following correct ? For which nodes does one have to check?
	//if(nodeInfo[0].opennessState == GeoNode::LABEL_CLOSE){
	//	if(mWasClicked){
	//		//Let it continue to be clicked and move the held object the given amount.
	//		Vec3_type fingerPos;
	//		for(int i=0; i<3; ++i)	
	//			fingerPos[i] = (&nodeInfo[0].positionWorld.x)[i];
	//		
	//		result.mMouseMovement = fingerPos - mLastPosition;
	//		result.mClickAction = HandMouseResult::HELD_CLICK;
	//		mLastPosition = fingerPos;
	//		return result;
	//	}
	//	//Start the click.
	//	mWasClicked = true;
	//	//Find position of hand 
	//	const Vec3_type zeroVector(0.0,0.0,0.0);
	//	result.mMouseMovement = zeroVector;
	//	result.mClickAction = HandMouseResult::START_CLICK;
	//	return result;
	//}
	
	std::vector<Idx_type> foundGeoNodeIndices;
	std::vector<Vec3_type> foundGeoNodePositions;
	auto fingerSuccess = getFingerData(foundGeoNodeIndices, foundGeoNodePositions, iGesture, iNumberOfNodesBothHands);
	if(!fingerSuccess){
		Vec3_type zeroVector(0.0,0.0,0.0);
		result.mMouseMovement = zeroVector;
		return result;
	}

	if(mLastGeoNodeTracked == invalid_idx){
		mLastGeoNodeTracked = foundGeoNodeIndices[0];
		mLastPosition = foundGeoNodePositions[0];
		Vec3_type zeroVector(0.0,0.0,0.0);
		result.mMouseMovement = zeroVector;
		return result;
	}

	//So hand is no longer closed check if it was previously closed 
	//if(mWasClicked){
	//	mWasClicked = false;
	//	//If the time during which it was clicked is less that 1 second then we interpret that as a single mouse click
	//	const auto& clickDuration = steady_clock::now() - mLastClosedHandTime;
	//	if(duration_cast<milliseconds>(clickDuration).count() < 1000){
	//		//The user just opened and closed their hand quickly so we interpret that as a mouse click.
	//		const Vec3_type zeroVector(0.0,0.0,0.0);
	//		result.mMouseMovement = zeroVector;
	//		result.mClickAction = HandMouseResult::SINGLE_CLICK;
	//		return result;
	//	}
	//	
	//	mLastGeoNodeTracked = invalid_idx;
	//	for(int i=0; i<3; ++i)
	//		mLastPosition[i] = 0.0;
	//	const Vec3_type zeroVector(0.0,0.0,0.0);
	//	result.mMouseMovement = zeroVector;
	//	result.mClickAction = HandMouseResult::STOP_CLICK;
	//	return result;
	//}

	//We interpret a mouse click as being a change in the number of fingers used. I.e. when we had one finger and we now 
	//have more than 1 then that is a click. Vice versa we ignore. Finger clicking works by using the index finger
	//to move the mouse cursor. To click the user uses their thumb.

	if(mNFingersTracked == 1 && foundGeoNodeIndices.size()>1){
		mNFingersTracked = foundGeoNodeIndices.size();
		const Vec3_type zeroVector(0.0,0.0,0.0);
		result.mMouseMovement = zeroVector;
		result.mClickAction = HandMouseResult::SINGLE_CLICK;
		return result;
	}

	mNFingersTracked = foundGeoNodeIndices.size();
	for(Idx_type i=0; i< foundGeoNodeIndices.size(); ++i){
		if(1 != foundGeoNodeIndices[i])
			continue;
		
		//Now we have found the same finger. If it has moved a large distance we interpret this as a click.
		const auto fingerMovement = foundGeoNodePositions[i] - mLastPosition;
		mLastPosition = foundGeoNodePositions[i];
		//So we previously had found a finger and it was the same one.
		result.mMouseMovement = fingerMovement;
		return result;
	}

	mLastGeoNodeTracked = foundGeoNodeIndices[0];
	mLastPosition = foundGeoNodePositions[0];
	Vec3_type zeroVector(0.0,0.0,0.0);
	result.mMouseMovement = zeroVector;
	return result;
}

} //namespace easymotion
