#ifndef HAND_MOUSE_H
#define HAND_MOUSE_H

#include "BasicTypes.h"
#include <pxcgesture.h>
//#include <chrono>

//Take over the OS's mouse. Based on what the user does, the class interprets the corresponding mouse operation. 
class PXCGesture;
namespace easymotion{
class HandMouse{
public:
	HandMouse();
	/// Takes as arguments the gesture info and number of nodes on the hands. Returns the movement
	/// of the mouse.
	HandMouseResult processUserFingers(PXCGesture *iGesture, const Idx_type iNumberOfNodesBothHands);

private:
	
	//The last Geonode (finger) that was found when tracking.
	Idx_type mLastGeoNodeTracked;
	//The position of the geonode that was found.
	Vec3_type mLastPosition;
	//To time how long the user "clicked" with his/her fingers
	//std::chrono::steady_clock::time_point mLastClosedHandTime;
	//The number of fingers previously tracked.
	Idx_type mNFingersTracked;

};

} // namespace easymotion
#endif