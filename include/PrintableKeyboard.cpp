#include "PrintableKeyboard.h"
#include "GeometricUtilityFunctions.h"
#include "OBBTreeCollide.h"
#include <fstream>
#include <sstream>
#include <string>
#include <iostream>

using namespace boost::filesystem;
using namespace std;

namespace easymotion{

PrintableKeyboard::PrintableKeyboard() : mKeyHeight((Scalar)keyboard_key_height) 
{
	mInvalidKey = invalid_key;
	mKeyBoardId = invalid_int;
	mKeyPressed = false;
}

void PrintableKeyboard::init(const PrintableKeyboard& iOther){
	mKeys = iOther.getKeys();
	mInvalidKey = iOther.getInvalidKey();
	mKeyBoardId = iOther.getKeyboardId();
	mOBBTree = iOther.getOBBTree();
	mKeyPressed = false;
}

PrintableKeyboard::PrintableKeyboard(const PrintableKeyboard& iOther) : mKeyHeight((Scalar)keyboard_key_height) {
	init(iOther);
}

PrintableKeyboard& PrintableKeyboard::operator=(const PrintableKeyboard& iOther){
	if (this == &iOther)      // Same object?
		return *this; 
	//Set values
	init(iOther);
	return *this;
}

bool PrintableKeyboard::initializeKeyboard(const path& iFileName, const int iKeyboardId){
	//Intialize the keyboard's key values etc. based on the XML file (?) that's been read. Return true if we succeed 
	//otherwise false. Also build the OBBTree.
	if(exists(iFileName)){
		//Read in all the keys as well as their positions and values. 
		//Open the file and start reading.
		ifstream file;
		file.exceptions(ios::failbit | ios::badbit);
		// Read binary/ascii tag
		file.open(iFileName.c_str(), ios::in);
		if(!file)
			return false;
		string line, token;
		//The following should already have been done so no need to do it again!
		getline(file, line);
		{
			istringstream input(line);
			//Get the first item in the string line.
			input >> token;
			if(token.compare("keyboard") != 0) 
				return false;
		}
		int markCounter = 0;
		while(file.peek() != ifstream::traits_type::eof()){
			getline(file, line);
			if (line == "") continue;
			istringstream input(line);
			//Create a key based on the input 
			Key key;
			bool isKey = true;
			//Get the next item in the string line.
			while(input >> token){
				//Fill in the value and location etc.
				if(token.compare("value") == 0){
					char tmpKeySymbol[256];
					input >> tmpKeySymbol;
					if(tmpKeySymbol[0] == '1')
						key.mValue = char(32);
					else
					key.mValue = tmpKeySymbol[0];
				}else
				if(token.compare("offset") == 0){
					char XOffset[256];
					input >> XOffset;
					key.mKeyConfig.center.x = (Scalar)atof(XOffset);
					char YOffset[256];
					input >> YOffset;
					key.mKeyConfig.center.y = (Scalar)atof(YOffset);
				}else
				if(token.compare("angle") == 0){
					char angle[256];
					input >> angle;
					key.mKeyConfig.angle = (Scalar)atof(angle);
				}else
				if(token.compare("size") == 0){
					char width[256];
					input >> width;
					key.mKeyConfig.size.width = (Scalar)atof(width);
					char height[256];
					input >> height;
					key.mKeyConfig.size.height = (Scalar)atof(height);
				}else
				if(token.compare("corner") == 0){
					Vec2_type tmpVec;
					char XOffset[256];
					input >> XOffset;
					tmpVec[0] = (Scalar)atof(XOffset);
					char YOffset[256];
					input >> YOffset;
					tmpVec[1] = (Scalar)atof(YOffset);
					mKeyboardCorners[markCounter] = tmpVec;
					++markCounter;
					isKey = false;
				}else
				if(token.compare("MarkerSize") == 0){
					char size[256];
					input >> size;
					mMarkerSize = (Scalar)atof(size);
					isKey = false;
				}
				else
					cout << "Unknown token" << endl;
			}
			if(isKey){
				mKeys.push_back(key);
			}
		}
		file.close();
	}

	Indexed_keys_vector keysWithIndex(mKeys.size());
	for(Idx_type i=0; i<keysWithIndex.size(); ++i){
		keysWithIndex[i].mKey = mKeys[i];
		keysWithIndex[i].mIndex = i;
	}

	if(!mOBBTree.buildTree(keysWithIndex))
		return false;
	setKeyboardId(iKeyboardId);
	return true;
}

bool PrintableKeyboard::keyboardCollide(KeyValue& oKeyValue, const FingerObject& iFinger, const Transformation& iKeyBoardTransf){
	//The way we test this is as follows we test first whether the user's fingers would
	//intersect an oriented bounding box around the keyboard piece of paper. Then if they
	//are we test whether they intersect the left half or the right half of the keyboard
	//and we then test against all keys in the intersected half.
	
	//Test the fingers against the keyboard.
	collideResult result;
	obbTreeCollide(result, mOBBTree, mKeyHeight, iFinger, iKeyBoardTransf);

	Scalar minDist = std::numeric_limits<Scalar>::max();
	for(Idx_type i=0; i< result.mCollidingKeys.size(); ++i){
		const auto& currentKey = mKeys[result.mCollidingKeys[i]];
		// Take squared distance because we only want to compare.
		const auto tmpDist = distance2ToKey(currentKey, iFinger, iKeyBoardTransf);
		if(tmpDist < minDist){
			minDist = tmpDist;
			oKeyValue = mKeys[result.mCollidingKeys[i]].getKeyValue();
		}
	}

	return result.mColliding;
}

KeyValue PrintableKeyboard::getKeyPressed(const FingerObject& iFinger, const Transformation& iKeyBoardTransf){
	//We think of the keys as being an oriented bounding box (OBB) each one located somewhere in space. 
	//We've built an OBBTree around all the keys and now we are going to test them for intersection.

	KeyValue pressedKeyValue = mInvalidKey;
	//See if a key was pressed.
	keyboardCollide(pressedKeyValue, iFinger, iKeyBoardTransf);
	//Return the closest pressed key to the user's finger if there was one.
	return pressedKeyValue;
}

void PrintableKeyboard::setKeyboardId(const int iId){
	mKeyBoardId = iId;
}
int PrintableKeyboard::getKeyboardId() const{
	return mKeyBoardId;
}
size_t PrintableKeyboard::getNKeys() const{
	return mKeys.size();
}
const std::vector<Key>& PrintableKeyboard::getKeys() const{
	return mKeys;
}
KeyValue PrintableKeyboard::getInvalidKey() const{
	return mInvalidKey;
}
const OBBTree& PrintableKeyboard::getOBBTree() const{
	return mOBBTree;
}
bool PrintableKeyboard::isValidKeyboard() const{
	return mKeyBoardId != invalid_int;
}
const Key& PrintableKeyboard::getKey(const int iIndex) const{
	return mKeys[iIndex];
}
Scalar PrintableKeyboard::getKeyHeight() const{
	return mKeyHeight;
}
void PrintableKeyboard::setKeyHeight(Scalar iHeight) {
	mKeyHeight = iHeight;
}

KeyValue PrintableKeyboard::getKeyValue(const int iKeyIndex) const{
	return mKeys[iKeyIndex].getKeyValue();
}
const std::array<Vec2_type, 4>& PrintableKeyboard::getKeyboardCorners() const{
	return mKeyboardCorners;
}
Scalar PrintableKeyboard::getMarkerSize() const{
	return mMarkerSize;
}

Idx_type PrintableKeyboard::getKeyValueIdx(const KeyValue iKeyValue) const{
	 for(Idx_type i=0; i<mKeys.size(); ++i)
		 if(iKeyValue == mKeys[i].getKeyValue())
			 return i;

	 return invalid_int;
}

}//namespace easymotion