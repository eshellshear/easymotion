#include "OBBTreeUtilities.h"
#include "GeometricUtilityFunctions.h"
#include <algorithm>

namespace easymotion{

void computeKeysDimensions(Vec2_type& oMax, Vec2_type& oMin, const Indexed_keys_vector& iIndexedKeys, const int iStart, const int iEnd){
	//Find max and min values over all keys.
	for(int i=iStart; i<iEnd; ++i){
		for(int j=0; j<4; ++j){
			const auto vertex = getRotatedRectVertex(iIndexedKeys[i].mKey.mKeyConfig, j);
			oMax[0] = std::max(oMax[0], vertex[0]);
			oMin[0] = std::min(oMin[0], vertex[0]);

			oMax[1] = std::max(oMax[1], vertex[1]);
			oMin[1] = std::min(oMin[1], vertex[1]);
		}
	}
}

Scalar computeSplitPoint(int& oAxis, const Vec2_type& iMax, const Vec2_type& iMin){
	//We choose to split on the axis with greatest variation and in the midpoint.
	if(iMax[0] - iMin[0] > iMax[1] - iMin[1]){
		oAxis = 0;
		return (iMax[0] + iMin[0])*(Scalar)0.5;
	}else{
		oAxis = 1;
		return (iMax[1] + iMin[1])*(Scalar)0.5;
	}
}

int computeSplitIndexAndSwap(const Vec2_type& iMax, const Vec2_type& iMin, Indexed_keys_vector& iIndexedKeys, const int iStart, const int iEnd){

	//First thing we do is to compute the split point and axis.
	Scalar splitPoint;
	int splitAxis;
	splitPoint = computeSplitPoint(splitAxis, iMax, iMin);

	//Now swap all leaves based on whether the center point is on the one side or the other.
	int counter = 0;
	for(int i=iStart;i<iEnd;++i) {
		const auto offset = iIndexedKeys[i].mKey.getOffset();
		if(offset[splitAxis] <= splitPoint) {
			std::swap(iIndexedKeys[i], iIndexedKeys[iStart+counter]);
			++counter;
		}
	}

	if((counter == 0) || (counter == iEnd-iStart))
		counter = (iEnd-iStart)/2;
	return iStart + counter;
}

}//namespace easymotion.