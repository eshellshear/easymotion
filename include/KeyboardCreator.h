#ifndef KEYBOARD_CREATOR_H	
#define KEYBOARD_CREATOR_H

#include <opencv2\core\types_c.h>
#include "MultiMarker.h"
#include "highgui.h"

#include "BasicTypes.h"

#include <string>
#include <sstream>

//using namespace std;
//using namespace alvar;

//This file is based on the SampleMarkerCreator so that we can creat a keyboard with a marker on it as well as keys.
//While creating the keyboard we create a png file that can be printed (the keyboard) as well as a specification of 
//the keyboard as an xml file.

namespace easymotion{
class KeyboardCreator{

	KeyboardCreator();
	~KeyboardCreator();

	//At the moment we will just create an american english style keyboard.
	bool createKeyboard(std::string& iKeyboardType);

private:
	bool createMarker(const char *iId);
	bool createKeys();
	bool saveKeyboard();

	IplImage *mKeyboardImg;
    std::stringstream filename;
    double minx, miny, maxx, maxy; // top-left and bottom-right in pixel units
    alvar::MultiMarker multi_marker;

    // General options
    bool   prompt;
    double units;           // how many pixels per one unit
    double marker_side_len; // marker side len in current units
    int    marker_type;     // 0:MarkerData, 1:ArToolkit
    double posx, posy;      // The position of marker center in the given units
    int    content_res;
    double margin_res;

    // MarkerData specific options
    alvar::MarkerData::MarkerContentType marker_data_content_type;
    bool                          marker_data_force_strong_hamming;

	// Because the layout of the keys will be fixed according to the type of keyboard that is
	// chosen we really only need to store the positions of the keys. However, for flexibility 
	// and safety we'll also store the unicode values.
	std::vector<KeyValue> mKeyValues;

	/// The vector of keys are the position, orientation and side lengths of each of the keys.
	std::vector<Key> mKeys;
};

}//namespace easymotion
#endif