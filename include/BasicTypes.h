#ifndef EASYMOTION_BASICTYPES_H
#define EASYMOTION_BASICTYPES_H

#include <vector>
#include <array>
#include "opencv2/core/core.hpp"
#include <limits>

namespace easymotion{

	typedef size_t Idx_type;
	typedef char KeyValue;
	typedef float Scalar;

	const Idx_type invalid_idx = std::numeric_limits<Idx_type>::infinity();
	const int invalid_int = INT_MAX;
	const char invalid_char = CHAR_MAX;
	const KeyValue invalid_key = invalid_char;
	const Scalar keyboard_key_height = 2.5;
	//Use the CV Point2f class because it comes with a bunch of extra functions like dot products, etc.
	typedef cv::Vec2i Vec2i_type;
	typedef cv::Vec2f Vec2_type;
	typedef cv::Vec3f Vec3_type;
	struct Vec2iXSorter{
		  inline bool operator() (const Vec2_type& iV, const Vec2_type& iW)
			{
				return (iV[0] > iW[0]);
			}
	};
	
	//Use the RotatedRect class because it contains a position, height and width and angle. 
	//Everything we need to define a single key.
	typedef cv::RotatedRect RotatedRect;
	//So we consider Rotation[0] as row 1, etc. I.e. each vector defines a row.
	struct Rotation{
		Rotation() {
			rotationIdentity();
		}

		inline const Vec3_type& operator[] (const int iIndex) const { assert(iIndex < 3); return rows[iIndex];}
		inline Vec3_type& operator[] (const int iIndex) { assert(iIndex < 3); return rows[iIndex];}

		inline Vec3_type getCol(const int iCol) const{
			Vec3_type tmp;
			tmp[0] = rows[0][iCol];
			tmp[1] = rows[1][iCol];
			tmp[2] = rows[2][iCol];
			return tmp;
		}

		inline void rotationIdentity(){
			rows[0][0] = 1.0;
			rows[0][1] = 0.0;
			rows[0][2] = 0.0;

			rows[1][0] = 0.0;
			rows[1][1] = 1.0;
			rows[1][2] = 0.0;

			rows[2][0] = 0.0;
			rows[2][1] = 0.0;
			rows[2][2] = 1.0;
		}

		std::array<Vec3_type,3> rows;
	};

	struct Transformation{
		Transformation()  {
			//Set Rotation to identity.
			mR.rotationIdentity();
			//Set translation to zero.
			mT[0] = 0.0;
			mT[1] = 0.0;
			mT[2] = 0.0;
		}
		Transformation(const Rotation& iR, const Vec3_type& iT) : mR(iR), mT(iT) {}
		Transformation(const Transformation& iTransf) {
			mR = iTransf.mR;
			mT = iTransf.mT;
		}

		Transformation& operator=(const Transformation& iOtherT){
			if (this == &iOtherT)      // Same object?
				return *this; 
			//Set values
			mR = iOtherT.mR;
			mT = iOtherT.mT;
			return *this;
		}

		//The 3D Rotation. It represents the orientation of a point.
		Rotation mR;
		//The 3D translation. It represents how far away the point is translated from its original position.
		Vec3_type	 mT;
	};

	//Should probably think about creating my own basic types such as rotations, transformations, etc. Could inherit 
	//the CvMat classes then will get a number of operations done for free. Or can use the vtk classes. Whichever are
	//easier.
	struct Key{
		Key() {}
		Key(const RotatedRect& iKeyConfig, const KeyValue iValue) : mKeyConfig(iKeyConfig), mValue(iValue) {}
		Key& operator=(const Key& iOtherKey){
			if (this == &iOtherKey)      // Same object?
				return *this; 
			//Set values
			mKeyConfig = iOtherKey.mKeyConfig;
			mValue = iOtherKey.mValue;
			return *this;
		}

		Scalar getXOffset() const {return mKeyConfig.center.x;}
		Scalar getYOffset() const {return mKeyConfig.center.y;}
		Vec2_type getOffset() const {
			Vec2_type tmpVec; 
			tmpVec[0] = mKeyConfig.center.x;
			tmpVec[1] = mKeyConfig.center.y;
			return tmpVec;
		}
		Scalar getAngle() const {return mKeyConfig.angle;}
		KeyValue getKeyValue() const {return mValue;}

		KeyValue mValue;
		RotatedRect mKeyConfig;
	};

	typedef std::vector<Key> keys_vector;

	struct IndexedKey{
		IndexedKey () {}
		IndexedKey(const Key& iKey, const int iIndex) : mKey(iKey), mIndex(iIndex) {}
		IndexedKey& operator=(const IndexedKey& iOtherIndexedKey){
			if (this == &iOtherIndexedKey)      // Same object?
				return *this; 
			//Set values
			mKey = iOtherIndexedKey.mKey;
			mIndex = iOtherIndexedKey.mIndex;
			return *this;
		}

		Key mKey;
		int mIndex;
	};

	typedef std::vector<IndexedKey> Indexed_keys_vector;
	typedef std::vector<int> Int_vector;

	struct FingerObject{
		easymotion::Vec3_type mFingerPosition;
		easymotion::Scalar mFingerRadius;
	};

	struct HandMouseResult{

		HandMouseResult () : mClickAction(NO_CLICK), mMouseMovement(0.0,0.0,0.0) {}

		//Did we just do a short click or long click or no click at all.
		enum ClickType{
			SINGLE_CLICK = 0,
			START_CLICK = 1,
			HELD_CLICK = 2,
			STOP_CLICK = 3,
			NO_CLICK = 4,
		};

		//What is the user doing with their hand? This encodes what is happening.
		ClickType mClickAction;

		//Mouse movement vector
		Vec3_type mMouseMovement;
	};
} //namespace easymotion


#endif