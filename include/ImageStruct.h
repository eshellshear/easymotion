#ifndef IMAGE_STRUCT_h
#define IMAGE_STRUCT_h

#include <string>
#include <opencv2\core\types_c.h>

namespace easymotion{
namespace helperClasses{
/** \brief Image structure to store the images internally */
struct Image {
	IplImage *ipl;
	std::string title;
	bool visible;
	bool release_at_exit;
	Image(IplImage *_ipl, std::string _title, bool _visible, bool _release_at_exit)
		:ipl(_ipl),title(_title),visible(_visible),release_at_exit(_release_at_exit) {}
};

} //helperClasses
} //easymotion
#endif