#define _WIN32_WINNT 0x0501
#include <windows.h>
#include "BasicTypes.h"

void MoveMouse(const easymotion::Vec3_type& iMovement);
void LeftClick();
void LeftHoldClick();
void LeftUnClick();
void SetMousePosition(POINT& mp);
POINT GetMousePosition();
void doKeyboardOutput(const easymotion::KeyValue iKeyValue);
