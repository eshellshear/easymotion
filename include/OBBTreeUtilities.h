#ifndef OBB_TREE_UTILITIES_H
#define OBB_TREE_UTILITIES_H

#include "BasicTypes.h"

namespace easymotion{
/// Compute the max and min x and y values of a set of keys (rotated rectangles).
void computeKeysDimensions(Vec2_type& oMax, Vec2_type& oMin, const Indexed_keys_vector& iIndexedKeys, const int iStart, const int iEnd);
/// Find the axis to split on and where.
Scalar computeSplitPoint(int& oAxis, const Vec2_type& iMax, const Vec2_type& iMin);
/// To find the split point amongst a bunch of keys in an OBBNode.
int computeSplitIndexAndSwap(const Vec2_type& iMax, const Vec2_type& iMin, Indexed_keys_vector& iIndexedKeys, int iStart, int iEnd);

}//namespace easymotion

#endif