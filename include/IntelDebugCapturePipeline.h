#ifndef INTEL_DEBUG_GESTUREPIPELINE_h
#define INTEL_DEBUG_GESTUREPIPELINE_h

//Alvar/OpenCv includes
#include "Alvar.h"
#include "cv.h"
#include "highgui.h"
#include "CaptureFactory.h"
#include "Pose.h"
#include "Camera.h"
//Intel includes
#include "util_render.h" // to render color and depth images
#include <util_pipeline.h>
#include <pxcgesture.h>
#include <pxcdefs.h>
#include <pxcprojection.h>
//#include "gesture_render.h"
//Std includes
#include <array>
#include <vector>
#include <string>
#include <memory>
#include <chrono>
#include <thread>
//My project includes
#include "BasicTypes.h"
#include "PrintableKeyboard.h"
#include "ImageStruct.h"
#include "HandMouse.h"

using std::chrono::steady_clock;
//So this pipeline is designed to go one level lower than the GesturePipeline so that we can intercept the
//depth camera and alter the depth info.
class IntelDebugCapturePipeline {
	typedef easymotion::KeyValue				KeyValue;
	typedef easymotion::Idx_type				Idx_type;
	typedef easymotion::Vec2i_type				Vec2i_type;
	typedef easymotion::Vec2iXSorter			Vec2iXSorter;
	typedef easymotion::Vec2_type				Vec2_type;
	typedef easymotion::Vec3_type				Vec3_type;
	typedef easymotion::Rotation				Rotation;
	typedef easymotion::Transformation			Transformation;
	typedef easymotion::FingerObject			FingerObject;
	typedef easymotion::PrintableKeyboard		PrintableKeyboard;
	typedef easymotion::HandMouse				HandMouse;
	typedef easymotion::helperClasses::Image	Image;

	typedef alvar::Camera						Camera;
	typedef alvar::Capture						Capture;
	typedef alvar::Pose							Pose;

public:
	IntelDebugCapturePipeline();
	~IntelDebugCapturePipeline();
	
	typedef PXCGesture::GeoNode GeoNode;

	enum Fingers{
		numberOfNodesSingleHand = 10,
		numberOfNodesBothHands = 20
	};

	enum{
		NCornerPoints = 4,
	};

	/// The function that does it all.
	bool startVirtualKeyboard();

private:
	/// Functions ///
	/// Find the keyboards on file and load them into the mKeyboards vector
	bool initializeKeyboards();
	/// Find the capture devices and and save one into the mCapture variable
	bool initializeCaptureDevice();
	/// Find where the keyboard and fingers are located to be able to compute the transformation to go between the
	/// camera and motion sensor
	bool getLocationInfo(Transformation& oKeyBTransf, std::array<Vec3_type, 4> oFingerPositions);
	/// Use the motion sensor to find the locations of the finger tips.
	bool getFingerInfo(std::vector<easymotion::FingerObject>& oFingerInfo, PXCGesture *iGesture, Pose& iPose);
	/// From a given image extract the pose of the marker (i.e. position and orientation).
	Pose getMarkerPose(IplImage *image);
	/// This function processes everything each frame.
	bool onNewFrame(UtilCapture& iCapture, PXCSmartPtr<PXCGesture>& iGesture, std::vector<UtilRender*>& iRenders, PXCSmartPtr<PXCProjection>& iProjection);
	/// This function gives me the 2D image coordinates of the printed keyboard
	PXCPointF32 getKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose, PXCSmartPtr<PXCProjection>& iProjection); 
	/// Get the 2D coordinates via Alvar functions
	PXCPointF32 getAlvarKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose);
	/// Get the 2D coordinates vis Intel functions. Return the position of the marker in the depth stream.
	PXCPointF32 getIntelKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose, PXCSmartPtr<PXCProjection>& iProjection);
	/// Get the transformation (orientation and position) of the keyboard
	bool getKeyboardTransf(Transformation& oKeyboardTransf, Pose& iPose);
	/// Get the pose of theIntel keyboard
	bool getKeyboardPose(Pose& oPose, IplImage* iImage);
	/// Get an IplImage from the RGB stream from the Intel camera. We convert the RGB stream into a cvMat and then convert that to an IplImage.
	IplImage* getImage(const PXCImage::ImageData& iColorData);
	/// Determine whether the user is using the keyboard or not
	bool useKeyboard(PXCSmartArray<PXCImage>& iImages,
						PXCSmartSPArray& iSps, 
						PXCSmartPtr<PXCGesture>& iGesture, 
						std::vector<UtilRender*>& iRenders, 
						PXCSmartPtr<PXCProjection>& iProjection);
	/// Determine whether the user is using the hand as a mouse
	bool useMouse(PXCSmartArray<PXCImage>& iImages,
					PXCSmartSPArray& iSps,
					PXCSmartPtr<PXCGesture>& iGesture);
	/// Release all controlled image resources.
	void releaseResources(PXCSmartArray<PXCImage>& iImages,
							PXCImage::ImageData& ioDepth_data, 
							PXCImage::ImageData& ioColor_data,
							IplImage* ioAlvarImage);
		
	//DEBUG FUNCTIONS AND VARIABLESS
	///Do a brute force collision computation between the finger and all the keys
	KeyValue bruteForceFind(const FingerObject& iFinger, const Transformation& iKeyBoardTransf);
	///Test function.
	std::vector<Vec3_type> keyPositions(const Transformation& iKeyBoardTransf);
	Vec3_type mHPosition;


    /// Variables ///
	/// The bool to inform whether the initialization of the capture devices succeeded or not.
    bool mCaptureSuccess;
    /// The bool is true if we successfully found a keyboard and loaded it. Otherwise false.
    bool mKeyboardSuccess;
	/// The transformation to go between the camera and motion sensor
	Transformation mCam2SensorTf;
	/// A bool to know whether we've already initialized the camera.
    bool mCameraInitialized;
	/// The camera object used for marker tracking.
	Camera mCamera;
	/// The name of the file where the calibration info for the camera is stored.
    std::stringstream mCalibrationFilename;
    /// Found Keyboards.
    std::vector<PrintableKeyboard> mKeyboards;
	/// The keyboard the user has chosen to use
	int mChosenKeyboard;
	/// Keep a record of the time key's are pressed by the user.
	std::chrono::steady_clock::time_point mLastKeyPressTime;
	/// The object that takes care of hand movements that are to be interpreted as mouse movements
	HandMouse mHandMouseObject;

	//Variables used in CvTestBedNew also used here
    /** \brief Capture device */
    Capture *mCapture;
   
	/** \brief Vector of images stored internally */
	std::vector<Image> mImages;

	/// To store the bad saturation and confidence values.
	std::array<pxcF32,2> mDepthBadValues;

};

#endif