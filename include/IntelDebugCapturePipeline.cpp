#include "IntelDebugCapturePipeline.h"

#include <opencv2/core/core.hpp>
#include "MarkerDetector.h"

#include "BasicMath.h"
#include "UtilityFunctions.h"
#include "OBBTreeCollide.h"
#include "GeometricUtilityFunctions.h"
#include "WINStuff.h"

#include <pxcimage.h>
#include <pxcmetadata.h>

//NOTE: Cannot write using namespace boost::filesystem because it causes Eigen to fail during compiling
namespace fs=boost::filesystem;
using namespace alvar;
using namespace std;
using namespace easymotion;
using std::chrono::duration_cast;
using std::chrono::milliseconds;

IntelDebugCapturePipeline::IntelDebugCapturePipeline() : 
	mCameraInitialized(false), 
	mCaptureSuccess(false),
	mKeyboardSuccess(false), 
	mCapture(nullptr), 
	mHandMouseObject()
{
}

IntelDebugCapturePipeline::~IntelDebugCapturePipeline(){
	if(mCapture){
		mCapture->stop();
		delete mCapture;
	}
}

bool IntelDebugCapturePipeline::initializeKeyboards(){
	
	//Most likely the keyboards should be located in the current directory.
	fs::path directoryName;
#if defined( _WIN32 ) || defined( _WIN64 )
	TCHAR szPath[MAX_PATH];
	GetModuleFileName(NULL, szPath, MAX_PATH);
	directoryName = szPath;
#endif
	//???
	//Remove the name of the executable:
	directoryName = directoryName.parent_path();

	//A list of all found keyboards.
	std::vector<fs::path> fileNames;
	const size_t NKeyboards = enumerateKeyboards(fileNames, directoryName);
	if(NKeyboards == 0)
		return false;
	mKeyboards.resize(NKeyboards);

	for(size_t i=0; i< NKeyboards; ++i){
		//All files containing the keyboard info will be located in a subdirectory in the executables directory called "Keyboard Layouts".
		if(!mKeyboards[i].initializeKeyboard(fileNames[i], i))
			std::cout << "Reading keyboard " << i << " failed" << std::endl;
	}

	bool someKeyboardSucceeded(false);
	for(size_t i=0; i< NKeyboards; ++i){
		someKeyboardSucceeded |= mKeyboards[i].isValidKeyboard();
	}
	if(!someKeyboardSucceeded)
		return false;

	//TODO: Let the user choose the keyboard they want (if they have a choice).
	mChosenKeyboard = 0;
	Scalar keyHeight = 2.7;
	mKeyboards[mChosenKeyboard].setKeyHeight(keyHeight);
	return true;
}

bool IntelDebugCapturePipeline::initializeCaptureDevice(){
	CaptureFactory::CapturePluginVector plugins = CaptureFactory::instance()->enumeratePlugins();
	if (plugins.size() < 1) {
		std::cout << "Could not find any capture plugins." << std::endl;
		return false;
	}
	
	CaptureFactory::CaptureDeviceVector devices = CaptureFactory::instance()->enumerateDevices();
	if (devices.size() < 1) {
		std::cout << "Could not find any capture devices." << std::endl;
		return false;
	}

	int selectedDevice = 0;
	mCapture = CaptureFactory::instance()->createCapture(devices[selectedDevice]);
	string uniqueName = devices[selectedDevice].uniqueName();

	if (!mCapture) 
		return false;

	std::stringstream settingsFilename;
	settingsFilename << "camera_settings_" << uniqueName << ".xml";
	mCalibrationFilename << "camera_calibration_" << uniqueName << ".xml";
            
	mCapture->start();
	mCapture->setResolution(640, 480);
            
	if (mCapture->loadSettings(settingsFilename.str())) {
		std::cout << "Loading settings: " << settingsFilename.str() << std::endl;
	}

	std::stringstream title;
	title << "SampleMarkerDetector (" << mCapture->captureDevice().captureType() << ")";

	return true;
}

Pose IntelDebugCapturePipeline::getMarkerPose(IplImage *image){
	bool flip_image = (image->origin?true:false);
	if (flip_image) {
		cvFlip(image);
		image->origin = !image->origin;
	}

	if (!mCameraInitialized){
		mCameraInitialized = true;
		mCamera.SetRes(image->width, image->height);
	}

	static MarkerDetector<MarkerData> marker_detector;
	//The next line defines the size of the edge of the marker in centimeters.
	const double marker_size= mKeyboards[mChosenKeyboard].getMarkerSize();
	marker_detector.SetMarkerSize(marker_size); 

	marker_detector.Detect(image, &mCamera, true, false);
	Pose p;
	for (size_t i=0; i<marker_detector.markers->size(); i++) {
		if (i >= 32) break;
        
		p = (*(marker_detector.markers))[i].pose;
	}
		
	return p;
}

bool IntelDebugCapturePipeline::startVirtualKeyboard(){
	// Create session
	PXCSmartPtr<PXCSession> session;
	pxcStatus sts = PXCSession_Create(&session);
	if (sts < PXC_STATUS_NO_ERROR) {
		wprintf(L"Failed to create a session\n");
		return false;
	}

	UtilCapture capture(session);

	// Set source device search critieria
	capture.SetFilter(L"DepthSense Device 325");
	PXCSizeU32 size_VGA = {640, 480};
	capture.SetFilter(PXCImage::IMAGE_TYPE_COLOR, size_VGA);
	PXCSizeU32 size_QVGA = {320, 240};
	capture.SetFilter(PXCImage::IMAGE_TYPE_DEPTH, size_QVGA);

	PXCSmartPtr<PXCGesture> gesture = nullptr;
	sts = session->CreateImpl<PXCGesture>(&gesture);
	if(sts < PXC_STATUS_NO_ERROR)
		return false;

	PXCGesture::ProfileInfo pinfo;
    sts=gesture->QueryProfile(0,&pinfo);
	pinfo.inputs.streams[1].format=PXCImage::COLOR_FORMAT_RGB32;
	pinfo.inputs.streams[1].sizeMin.width=pinfo.inputs.streams[1].sizeMax.width=640;
	pinfo.inputs.streams[1].sizeMin.height=pinfo.inputs.streams[1].sizeMax.height=480;

	sts = capture.LocateStreams (&pinfo.inputs);
	if (sts < PXC_STATUS_NO_ERROR) {
		wprintf(L"Failed to locate color and depth streams\n");
		return 1;
	}

	sts=gesture->SetProfile(&pinfo);
    if (sts<PXC_STATUS_NO_ERROR) 
		return false;

	PXCCapture::Device* device = capture.QueryDevice();

	PXCCapture::DeviceInfo device_info;
	sts = device->QueryDevice(&device_info);
	if(sts < PXC_STATUS_NO_ERROR)
		return false;

	// set the desired value for smoothing the depth data
	sts = capture.QueryDevice()->SetProperty(PXCCapture::Device::PROPERTY_DEPTH_SMOOTHING, 1);
	//Choose a value from 1 to 32767.
	sts = capture.QueryDevice()->SetProperty(PXCCapture::Device::PROPERTY_DEPTH_CONFIDENCE_THRESHOLD, 5000);
	
	capture.QueryDevice()->QueryProperty(PXCCapture::Device::PROPERTY_DEPTH_LOW_CONFIDENCE_VALUE, &mDepthBadValues[0]);
	capture.QueryDevice()->QueryProperty(PXCCapture::Device::PROPERTY_DEPTH_SATURATION_VALUE, &mDepthBadValues[1]);

	std::vector<UtilRender*> renders;
	for (int idx=0;;idx++) {
		PXCCapture::VideoStream *stream_v = capture.QueryVideoStream(idx);
		if (stream_v) {
			PXCCapture::Device::StreamInfo sinfo;
			sts = device->QueryStream(idx, &sinfo);

			WCHAR stream_name[256];
			switch (sinfo.imageType) {
				case PXCImage::IMAGE_TYPE_COLOR:
					swprintf_s(stream_name, L"Stream#%d (Depth)", idx);
					renders.push_back(new UtilRender(stream_name));
					break;
				case PXCImage::IMAGE_TYPE_DEPTH:
					swprintf_s(stream_name, L"Stream#%d (Color)", idx);
					renders.push_back(new UtilRender(stream_name));
					break;
				default:
					break;
			}
		}
		else
			break;
	}

	while(renders.size() < 2){
		WCHAR stream_name[256];
		swprintf_s(stream_name, L"Stream#%d (Depth)", renders.size());
		renders.push_back(new UtilRender(stream_name));
	}

	PXCSmartPtr<PXCProjection> projection;
	pxcUID prj_value;               // projection serializable identifier
	sts=capture.QueryDevice()->QueryPropertyAsUID(PXCCapture::Device::PROPERTY_PROJECTION_SERIALIZABLE,&prj_value);
	if(sts < PXC_STATUS_NO_ERROR)
		return false;
	session->DynamicCast<PXCMetadata>()->CreateSerializable<PXCProjection>(prj_value, &projection);

	//Do keyboard stuff.
	//Start initializing the capture device, keyboards and finding the transformation to go between 
	//devices. Note that using capture devices via Alvar and Intel at the same time won't work.
	bool useAlvarCapture = false;
	if(useAlvarCapture){
		if(!mCaptureSuccess)
			mCaptureSuccess = initializeCaptureDevice();
	}else{
		mCaptureSuccess = true;
		mCalibrationFilename << "camera_calibration_highgui_0.1xml";
	}
	
	if(!mKeyboardSuccess)
		mKeyboardSuccess = initializeKeyboards();
	
	//We return true here because otherwise the device will stop capturing images/depth info.
	if(!mCaptureSuccess || !mKeyboardSuccess )
		return false;
	
	mLastKeyPressTime = steady_clock::now();
	//The loop where we do the processing.
	for (;;) {
		onNewFrame(capture, gesture, renders, projection);
	}
}

void IntelDebugCapturePipeline::releaseResources(PXCSmartArray<PXCImage>& iImages,
													PXCImage::ImageData& ioDepth_data, 
													PXCImage::ImageData& ioColor_data,
													 IplImage* ioAlvarImage){

	if(ioAlvarImage)
		cvReleaseImageHeader(&ioAlvarImage);
	iImages[1]->ReleaseAccess(&ioColor_data);
	iImages[0]->ReleaseAccess(&ioDepth_data);
}

bool IntelDebugCapturePipeline::useKeyboard(PXCSmartArray<PXCImage>& iImages,
											PXCSmartSPArray& iSps, 
											PXCSmartPtr<PXCGesture>& iGesture, 
											std::vector<UtilRender*>& iRenders, 
											PXCSmartPtr<PXCProjection>& iProjection){

	// Get access to depth image
	PXCImage::ImageData depth_data; // pitch, planes
	iImages[0]->AcquireAccess(PXCImage::ACCESS_READ, &depth_data);
	PXCImage::ImageInfo depth_info; // width, height
	iImages[0]->QueryInfo(&depth_info);

	PXCImage::ImageData color_data; // pitch, planes
	iImages[1]->AcquireAccess(PXCImage::ACCESS_READ, PXCImage::COLOR_FORMAT_RGB32, &color_data);
	PXCImage::ImageInfo color_info; // width, height
	iImages[1]->QueryInfo(&color_info);

	//Steps to alter the depth stream
	//1) Find the corner points of the piece of paper in the RGB image via the pose of the piece of paper. 
	//2) Find the corresponding points in the depth image and assume that the corners aren't covered by hands. 
	//3) Find all points in the rectangle containing those corner points
	//4) Check the depth of all points in the rectangle and compare these values to the bilinear interpolation of all 
	//corner points. If they are roughly the same then set the depth value to -infinity. Otherwise leave values alone.

	//Get the image for use.
	IplImage* alvarImage = getImage(color_data);

	// The transformation to locate the current position of the keyboard
	Pose pose;
	if(!getKeyboardPose(pose, alvarImage)){
		releaseResources(iImages, depth_data, color_data, alvarImage);
		return false;
	}

	std::array<PXCPointF32, NCornerPoints> keyboardDepth2DCorners;
	const auto marker2DPos = getKeyboardCorners(keyboardDepth2DCorners, pose, iProjection);

	std::array< Vec2i_type, NCornerPoints> depthCornerPoints;
	for(Idx_type i=0; i<NCornerPoints; ++i){
		depthCornerPoints[i][0] = (int)keyboardDepth2DCorners[i].x;
		depthCornerPoints[i][1] = (int)keyboardDepth2DCorners[i].y;
	}

	///Sort points so that the first 2 points are those with smallest x value then sort them so that the first 2 points have
	// smallest x value and next two points have largest x value.
	orderCornerPoints(depthCornerPoints);

	//Render corner points on depth picture.
	auto depth_width = depth_info.width;
	auto depth_height = depth_info.height;

	std::array< int, NCornerPoints> cornerDepths;

	// Get the depths of all the depthCornerPoints
	
	for(Idx_type i=0; i<NCornerPoints; ++i){
		//If the depth values are bad then we search for a good depth value near the current location.
		const auto index = (depthCornerPoints[i][1])*depth_width+(depthCornerPoints[i][0]);
		pxcU16 depth = (pxcU16)mDepthBadValues[0];
		if(index <depth_width*depth_height)
			depth = ((pxcU16*)depth_data.planes[0])[index];
		//if(depth != (pxcU16)mDepthBadValues[0] && depth != (pxcU16)mDepthBadValues[1]){
		//	for(Idx_type i=0; i<NCornerPoints; ++i)
		//		cornerDepths[i]=depth;
		//	break;
		//}
		
		//Could also try making the depth value of all 8 neighbours of the current depth value as long as their values 
		//aren't invalid.

		if(depth == (pxcU16)mDepthBadValues[0] || depth == (pxcU16)mDepthBadValues[1]){
			cout << "Bad value " << i << endl;
			if(!getNewDepthValue(cornerDepths[i], depth_data.planes[0], depth_width, depth_height, depthCornerPoints[i], mDepthBadValues))
				if(i>0)
					cornerDepths[i] = cornerDepths[i-1];
				else
					cornerDepths[i] = std::numeric_limits<int>::infinity();
				//return true;
		}else
			cornerDepths[i]=depth;
	}

	if(cornerDepths[0] == std::numeric_limits<int>::infinity())
		cornerDepths[0] = cornerDepths[3];

	//smoothKeyboardArea(depthCornerPoints, depth_data.planes[0], depth_height, depth_width);

	std::vector< Vec2i_type > interiorPoints;
	rectangleInteriorPoints(interiorPoints, depthCornerPoints);
	assert(interiorPoints.size() > 0);

	//Now go through the depth stream and check depth value of all interior points.
	Vec3_type planeNormal;
	Scalar planeValue;
	getPlaneNormalAndScalar(planeNormal, planeValue, depthCornerPoints, cornerDepths);

	for(Idx_type i=0; i<interiorPoints.size(); ++i){
		auto depthIndex = interiorPoints[i][0] + interiorPoints[i][1]*depth_width;
		depthIndex = std::min(depthIndex, depth_width*depth_height-1);
		pxcU16 depth = ((pxcU16*)depth_data.planes[0])[depthIndex];
		if(depth == (pxcU16)mDepthBadValues[0] || depth == (pxcU16)mDepthBadValues[1]){
			((pxcU16*)depth_data.planes[0])[depthIndex] = (pxcU16)0xFFFFFFFF;
			continue;
		}

		//const auto paperDepth = bilinearInterpolationFloor(depthCornerPoints, cornerDepths, interiorPoints[i]);
		const auto paperDepth = (int)getPlaneDepth(planeNormal, planeValue, interiorPoints[i]);

		const auto lowerDepth = paperDepth -10;

		//Remove all depth coordinates less than the plane to make it easier to track the fingers.
		if(depth >= lowerDepth)
			//Set depth value to max.
			((pxcU16*)depth_data.planes[0])[depthIndex] = (pxcU16)0xFFFFFFFF;
	}

	iGesture->ProcessImageAsync(iImages,iSps.ReleaseRef(1));
	iSps.SynchronizeEx();
	auto sts=iSps[0]->Synchronize(0);
	if (sts < PXC_STATUS_NO_ERROR) {
		releaseResources(iImages, depth_data, color_data, alvarImage);
		return true;
	}
	
	//An array of ten FingerObjects for the ten fingers.
	std::vector<easymotion::FingerObject> fingerObjects;
	if(!getFingerInfo(fingerObjects, iGesture, pose)){
		releaseResources(iImages, depth_data, color_data, alvarImage);
		return true;
	}

	Transformation keyboardTransf;
	if(!getKeyboardTransf(keyboardTransf, pose)){
		releaseResources(iImages, depth_data, color_data, alvarImage);
		return false;
	}

	bool usingKeyboard = true;
	/// By what factor the height of the keyboard extends over the keys.
	const Scalar keyboardExpansionFactor = 4.0;
	//Now run the collision routine and return the keys that the fingers are colliding with.
	for(Idx_type i=0; i<fingerObjects.size(); ++i){
		//I seem to have to reverse the y-coordinate of the finger position.
		fingerObjects[i].mFingerPosition[2] *= -1.0;

		if(distanceToPlane(planeNormal, planeValue, fingerObjects[i].mFingerPosition) > keyboardExpansionFactor*mKeyboards[mChosenKeyboard].getKeyHeight())
			continue;

		usingKeyboard = true;
		//auto keyValue = bruteForceFind(fingerObjects[i], keyboardTransf);
		auto keyValue = mKeyboards[mChosenKeyboard].getKeyPressed(fingerObjects[i], keyboardTransf);

		if(keyValue == mKeyboards[mChosenKeyboard].getInvalidKey())
			continue;

		//Make sure that a second has gone before we press another key.
		auto lastkeypress = steady_clock::now() - mLastKeyPressTime;
		if(duration_cast<milliseconds>(lastkeypress).count() < 600)
			continue;
		else{ 
			mLastKeyPressTime = steady_clock::now();
		}
		doKeyboardOutput(keyValue);
	}

	if (iRenders[0]) {
		if (!iRenders[0]->RenderFrame(iImages[0])){
			delete iRenders[0];
			iRenders[0]=0;
		}
	}

	releaseResources(iImages, depth_data, color_data, alvarImage);
	return usingKeyboard;
}

bool IntelDebugCapturePipeline::useMouse(PXCSmartArray<PXCImage>& iImages,
											PXCSmartSPArray& iSps,
											PXCSmartPtr<PXCGesture>& iGesture){
	iGesture->ProcessImageAsync(iImages,iSps.ReleaseRef(1));
	iSps.SynchronizeEx();
	auto sts=iSps[0]->Synchronize(0);

	//Find where fingers are and whether the mouse was used in the last frame. 
	//If so then move the mouse the difference of the finger positions.
	auto result = mHandMouseObject.processUserFingers(iGesture, numberOfNodesBothHands);

	//The Y movement is in the result.mMouseMovement[2] value so I need to swap them
	std::swap(result.mMouseMovement[1], result.mMouseMovement[2]);

	WINprocessResult(result);
	return true;
}

bool IntelDebugCapturePipeline::onNewFrame(UtilCapture& iCapture, PXCSmartPtr<PXCGesture>& iGesture, std::vector<UtilRender*>& iRenders, PXCSmartPtr<PXCProjection>& iProjection){
	//If one of them does not succeed we return false so that we can repeat the loop.
	PXCSmartArray<PXCImage> images(2);
	PXCSmartSPArray sps(2);
	// Get samples from input device and pass to the gesture module
	auto sts = iCapture.ReadStreamAsync(images.ReleaseRefs(),sps.ReleaseRef(0));
	if(sts < PXC_STATUS_NO_ERROR)
		return true;

	sts = sps[0]->Synchronize();
	if(sts < PXC_STATUS_NO_ERROR)
		return true;

	if(useKeyboard(images, sps, iGesture, iRenders, iProjection))
		return true;

	if (iRenders[0]) {
		if (!iRenders[0]->RenderFrame(images[0])){
			delete iRenders[0];
			iRenders[0]=0;
		}
	}

	if (iRenders[1]) {
		if (!iRenders[1]->RenderFrame(images[1])){
			delete iRenders[1];
			iRenders[1]=0;
		}
	}

	//Check whether user wants to use the mouse.,
	auto mouseSuccess = useMouse(images, sps, iGesture);
		
	return mouseSuccess;
}

std::vector<Vec3_type> IntelDebugCapturePipeline::keyPositions(const Transformation& iKeyBoardTransf){

	const auto& obbTree = mKeyboards[mChosenKeyboard].getOBBTree();
	const auto& obbNodes = obbTree.getNodes();

	std::vector<Vec3_type> keys;
	for(size_t i=0;i<obbNodes.size();++i){
		const auto& node = obbNodes[i];
		if(!node.isLeaf())
			continue;

		RotatedRect rect;
		rect.angle = (Scalar)0.0;
		rect.center = node.getOffset();
		rect.size.width = (Scalar)2.0*node.getHW()[0];
		rect.size.height = (Scalar)2.0*node.getHW()[1];

		Vec3_type offset;
		getRectOffset(offset, rect.center, iKeyBoardTransf.mR);
	
		Vec3_type OBBCenter = iKeyBoardTransf.mT;
		OBBCenter += offset;

		//Now convert everything to Intel's coordinates etc.
		//OBBCenter = convertToIntel(OBBCenter);
		const Scalar metersToCentimeters = (Scalar)0.01;
		for(Idx_type i=0;i<3;++i)
			OBBCenter[i] *= metersToCentimeters; 

		keys.push_back(OBBCenter);
		
	}
	return keys;
}

KeyValue IntelDebugCapturePipeline::bruteForceFind(const FingerObject& iFinger, const Transformation& iKeyBoardTransf){

	const auto& obbTree = mKeyboards[mChosenKeyboard].getOBBTree();
	const auto& obbNodes = obbTree.getNodes();
	const auto keyHeight = mKeyboards[mChosenKeyboard].getKeyHeight();
	const auto& keys = mKeyboards[mChosenKeyboard].getKeys();
	for(size_t i=0;i<obbNodes.size();++i){
		const auto& node = obbNodes[i];
		if(!node.isLeaf())
			continue;

		RotatedRect rect;
		rect.angle = (Scalar)0.0;
		rect.center = node.getOffset();
		rect.size.width = (Scalar)2.0*node.getHW()[0];
		rect.size.height = (Scalar)2.0*node.getHW()[1];

		if(collide(rect, keyHeight, iFinger, iKeyBoardTransf))
			return keys[node.getKeyIndex()].getKeyValue();
	}

	return mKeyboards[mChosenKeyboard].getInvalidKey();
}

IplImage* IntelDebugCapturePipeline::getImage(const PXCImage::ImageData& iColorData){
	//Create a cvMat out of iColorData then simply use assignment operator to create the iImage.
	IplImage* image = cvCreateImageHeader(cvSize(640, 480), 8, 4);
	cvSetData(image, (uchar*)iColorData.planes[0], 640*4*sizeof(uchar));
	return image;
}

bool IntelDebugCapturePipeline::getKeyboardPose(Pose& oPose, IplImage* iImage){
	if(!iImage){
		//IplImage *image = mCapture->captureImage();
		iImage = mCapture->captureImage();
		if (!iImage) 
			return false;
	}

	oPose = getMarkerPose(iImage);
	const auto& poseTranslation = oPose.translation;
	if(poseTranslation[0] != 0.0 || poseTranslation[1] != 0.0 || poseTranslation[2] != 0.0)
		return true;

	return false;
}

bool IntelDebugCapturePipeline::getKeyboardTransf(Transformation& oKeyboardTransf, Pose& iPose){
	oKeyboardTransf = getTransfFromPose(iPose);
	return true;
}

bool IntelDebugCapturePipeline::getFingerInfo(std::vector<easymotion::FingerObject>& oFingerInfo, 
											  PXCGesture *iGesture, Pose& iPose){
	//We want the positions of all the fingers. Can get this from the mNodeInfo function. The  5 here is for the
	//number of fingers.
	
	std::vector<Idx_type> foundGeoNodeIndices;
	std::vector<Vec3_type> foundGeoNodePositions;
	auto fingerSuccess = getFingerData(foundGeoNodeIndices, foundGeoNodePositions, iGesture, numberOfNodesBothHands);
	if(!fingerSuccess)
		return false;

	//Now copy over the info to the iFingerInfo array:
	oFingerInfo.resize(foundGeoNodePositions.size());
	for(size_t i=0; i<foundGeoNodePositions.size(); ++i){
		oFingerInfo[i].mFingerPosition = foundGeoNodePositions[i];
		oFingerInfo[i].mFingerRadius = (Scalar)0.015; //nodeInfo[i].radiusWorld;
	}
	return true;
}

PXCPointF32 IntelDebugCapturePipeline::getKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose, PXCSmartPtr<PXCProjection>& iProjection){
	const bool useAlvarFunctions = false;
	if(useAlvarFunctions)
		return getAlvarKeyboardCorners(o2DCorners, iPose);
	else
		return getIntelKeyboardCorners(o2DCorners, iPose, iProjection);

}
PXCPointF32 IntelDebugCapturePipeline::getIntelKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose, PXCSmartPtr<PXCProjection>& iProjection){

	std::vector<Vec3_type> threeDCorners(NCornerPoints);
	//Find the 3D corners from the keyboard information. It will be basically the position of the 
	//pose plus the halfwidth of the keyboard in the x direction plus the distance to bottom of the
	//keyboard and minus the distance to the top. Vice versa for the other direction.
	auto keyboard2DCorners = mKeyboards[mChosenKeyboard].getKeyboardCorners();
	auto keyboardTransf = getTransfFromPose(iPose);
		
	//HACK: GET RID OF THIS!!
	easymotion::Rotation keyboardRotation(keyboardTransf.mR);
	//for(int i=0; i<3;++i)
	//	for(int j=0; j<3;++j)
	//		keyboardRotation[i][j] = std::abs(keyboardTransf.mR[i][j]);

	for(Idx_type i=0; i<NCornerPoints; ++i)
		get3DPointFrom2d(threeDCorners[i], keyboardTransf.mT, keyboard2DCorners[i], keyboardRotation/*keyboardTransf.mR*/);

	//Need to rotate all the points because the intel 3D coordinate system is so that when facing the camera the x-axis points to the
	//right of the camera y axis points away from the camera in front of the laptop and z axis points upwards from the camera. 
	//The coordinate system is described in the Intel documentation under "SDK Manuals", "Programming Guide" then "Raw Stream 
	//Capturing and Processing" then finally "Coordinates and Projection". 
	//The current marker coordinate system is such that (x is to east, y is to north, and z is up from the marker) see MarkerDetector.h.
	//easymotion::Rotation alvarToIntelRotation;
	//for(int i=0; i<3;++i)
	//	for(int j=0; j<3;++j)
	//		alvarToIntelRotation[i][j] = 0.0;

	//alvarToIntelRotation[0][0] = 1.0;
	//alvarToIntelRotation[1][2] = 1.0;
	//alvarToIntelRotation[2][1] = 1.0;

	//std::vector<Vec3_type> correctedThreeDCorners(NCornerPoints);
	//for(Idx_type i=0; i<NCornerPoints; ++i)
	//	rotationMult(correctedThreeDCorners[i], alvarToIntelRotation, threeDCorners[i]);

	std::vector<PXCPoint3DF32> intel3DCorners(NCornerPoints);
	for(Idx_type i=0;i<NCornerPoints;++i){
		intel3DCorners[i].x = threeDCorners[i][0];
		intel3DCorners[i].y = threeDCorners[i][1];
		intel3DCorners[i].z = threeDCorners[i][2];
	}

	//In the keyboard text file I specify the keyboard dimensions in centimetres. So here I have to convert that to metres as stated 
	//In the above "Coordinates and Projection Guide".
	const Scalar metersToCentimeters = (Scalar)0.01;
	for(Idx_type i=0;i<NCornerPoints;++i){
		intel3DCorners[i].x *= metersToCentimeters; 
		intel3DCorners[i].y *= metersToCentimeters;
		intel3DCorners[i].z *= metersToCentimeters;
	}

	std::vector<PXCPointF32> depthCorners(NCornerPoints);
	auto sts= iProjection->ProjectRealWorldToImage(NCornerPoints, &intel3DCorners.front(), &depthCorners.front());

	for(Idx_type i=0;i<NCornerPoints;++i){
		o2DCorners[i].x = std::max((pxcF32)depthCorners[i].x, (pxcF32)0.0);
		o2DCorners[i].y = std::max((pxcF32)depthCorners[i].y, (pxcF32)0.0);
	}

	PXCPoint3DF32 markerIntelPos;
	markerIntelPos.x = keyboardTransf.mT[0];
	markerIntelPos.y = keyboardTransf.mT[1];
	markerIntelPos.z = keyboardTransf.mT[2];

	markerIntelPos.x *= metersToCentimeters; 
	markerIntelPos.y *= metersToCentimeters;
	markerIntelPos.z *= metersToCentimeters;

	PXCPointF32 markerPos;
	iProjection->ProjectRealWorldToImage(1, &markerIntelPos, &markerPos);
	return markerPos;
}

PXCPointF32 IntelDebugCapturePipeline::getAlvarKeyboardCorners(std::array<PXCPointF32, NCornerPoints>& o2DCorners, Pose& iPose){

	std::vector<Vec3_type> threeDCorners(NCornerPoints);
	//Find the 3D corners from the keyboard information. It will be basically the position of the 
	//pose plus the halfwidth of the keyboard in the x direction plus the distance to bottom of the
	//keyboard and minus the distance to the top. Vice versa for the other direction.
	auto keyboard2DCorners = mKeyboards[mChosenKeyboard].getKeyboardCorners();
	auto keyboardTransf = getTransfFromPose(iPose);
		
	for(Idx_type i=0; i<NCornerPoints; ++i)
		get3DPointFrom2d(threeDCorners[i], keyboardTransf.mT, keyboard2DCorners[i], keyboardTransf.mR);

	//Use the camera function ProjectPoints to get the 2D coordinates.
	std::vector<CvPoint3D64f> cv3DPoints(NCornerPoints);
	for(Idx_type i=0;i<NCornerPoints;++i){
		cv3DPoints[i].x = threeDCorners[i][0];
		cv3DPoints[i].y = threeDCorners[i][1];
		cv3DPoints[i].z = threeDCorners[i][2];
	}

	std::vector<CvPoint2D64f> twoDPoints(NCornerPoints);
	mCamera.ProjectPoints(cv3DPoints, &iPose, twoDPoints);

	for(Idx_type i=0;i<NCornerPoints;++i){
		o2DCorners[i].x = (pxcF32)twoDPoints[i].x;
		o2DCorners[i].y = (pxcF32)twoDPoints[i].y;
	}

	PXCPointF32 markerPos;
	return markerPos;
}