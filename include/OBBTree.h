#ifndef OBBTREE_h
#define OBBTREE_h

#include "BasicTypes.h"
#include "OBBNode.h"
#include <array>
#include <vector>

//We will define an OBBTreeNode and an OBBTreeModel. So the model will store all the nodes and indices to the keys.
namespace easymotion{

/// This class stores all the information about the OBBTree structure. It also has a build function to be able to build the tree and collide 
/// an object with the tree.

class OBBTree{
	typedef std::vector<OBBNode> OBB_node_vector;

public:
	OBBTree();
	OBBTree(const OBBTree& iOtherTree);
	OBBTree& operator=(const OBBTree& iOtherTree); 

	/// This is a special build function that splits keys etc. in a special way to force (initially) axis aligned OBBs.
	bool buildTree(const Indexed_keys_vector& iKeys);

	void setKeyIndex(const int iKeyIndex, const int iNodeIndex);
	const int getKeyIndex(const int iNodeIndex) const;
	const OBBNode& getOBBNode(const int iNodeIndex) const;
	const OBB_node_vector& getNodes() const;

	/// Check whether we've allocated the nodes. If we have then we assume that we've built the hierarchy.
	bool isBuilt() const;

private:

	void buildRecurse(Indexed_keys_vector& iIndexedKeys, const int iStart, const int iEnd, int& iNodeIndex);

	OBB_node_vector mNodes;
};

} //namespace easymotion

#endif