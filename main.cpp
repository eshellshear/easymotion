#include <iostream>

#define INTELMOTIONSENSOR

#ifdef INTELMOTIONSENSOR
//#include "IntelGesturePipeline.h"
//#include "IntelDebugGesturePipeline.h"
#include "IntelDebugCapturePipeline.h"
#endif


//A blocking main instead of the callback main as before. So we don't use the CvTestBedNew class like before we simply get the
//image from the capture device and process it ourselves.
int main(int argc, char *argv[]){
	try {
		IntelDebugCapturePipeline capturePipeline;
		capturePipeline.startVirtualKeyboard();
		return 0;
	}
	catch (const std::exception &e) {
		std::cout << "Exception: " << e.what() << std::endl;
	}
	catch (...) {
		std::cout << "Exception: unknown" << std::endl;
	}

}