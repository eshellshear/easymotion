To run the code you need the following packages:
OpenCV 2.4.0
Intel Perceptual Computing SDK
Intel/SoftKinetic's RGB-D Camera
glut-3.7.6-bin (i.e. just glut.h and glut32.dll)
Boost including the stage\lib files

If C:\Windows\Users\Keyboard is where your project is located then install OpenCV in C:\Windows\Users\OpenCV.

If C:\Windows\Users\Keyboard is where your project is located then install glut-3.7.6 in C:\Windows\Users\glut-3.7.6-bin

If C:\Windows\Users\Keyboard is where your project is located then install boost in C:\Windows\Users\boost

Where everything should be located can be seen from the project settings.

Note that it has only been tested for 32 bits and in Debug.

The solution file is for Visual Studio 2012.

The ALVAR code is subject to the license present at http://virtual.vtt.fi/virtual/proj2/multimedia/alvar/. The rest of the code is
 released under a MIT license as below:

// The MIT License (MIT)
//
// Copyright (c) 2015 Evan Shellshear
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.